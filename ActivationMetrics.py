import numpy as np
from numpy.core.umath_tests import inner1d
from data_processing import conceptors
import time

# [to-do] using pickle temporarily, but h5py would be more secure and spatially efficient
import pickle

def get_key_from_val(target_val, inp_dict):
    for key in inp_dict.keys():
        if inp_dict[key] == target_val:
            return key

class ActivationMetrics():

    def __init__(self, 
                 data, 
                 class_idx_dict, 
                 filepath_export=None, 
                 metadata=None):
        
        self.class_dict = class_dict
        self.calculate_Cross_correlation()

        if filepath_export is not None:
            self.export_Instance_data(filepath_export, metadata=metadata)

    def calculate_Trace_matrix(self):
        class_keys = self.data.keys()
        trace_vector = []*len(class_keys)
        # R is a list of cross correlation matrixes
        R = []*len(class_keys)
        print('beginning correlation matrix calculations ' + time.ctime())
        start_time = time.time()
        for k in class_keys:
            activations = self.data[k]
            for layer_key in activations.keys():
                idx = int(class_idx_dict[layer_key])
                R[idx] = dp.conceptors.calc_R(activations[layer_key])
            elapsed_time = (time.time() - start_time)/60
        print('correlation calculations completed in %i minutes\n' % int(elapsed_time))
        
        print('beginning trace matrix calculations ' + time.ctime())
        start_time = time.time()
        for i in range(len(R)):
            for j in range(i, len(R)):
                trace_matrix[i,j] = np.sum(inner1d(R[i],R[j]))
                trace_matrix[j,i] = trace_matrix[i,j]
            elapsed_time = (time.time() - start_time)/60
            print('completed %ith row after %i minutes' % (i, elapsed_time))
        
        self.trace_matrix = trace_matrix

        if filepath_export is not None:
            self.export_Instance_data(self, filepath_export, metadata)

    def export_Instance_data(self, filepath_export, metadata=None):
        if metadata is not None:
            self.meta_data = meta_data

        with open(filepath_export, 'wb') as f:
            pickle.dump(self, f)

    def load_Instance_data(self, filepath_import):
        with open(filepath_import, 'rb') as f:
            return pickle.load(f)
