'''Trains a simple convnet on the MNIST dataset.
Gets to 99.25% test accuracy after 12 epochs
(there is still a lot of margin for parameter tuning).
16 seconds per epoch on a GRID K520 GPU.
'''

from __future__ import print_function
import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K

import pdb

batch_size = 128
num_classes = 10
epochs = 1

# input image dimensions
img_rows, img_cols = 28, 28

# the data, split between train and test sets
(x_train, y_train), (x_test, y_test) = mnist.load_data()

if K.image_data_format() == 'channels_first':
    x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
    x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
    input_shape = (1, img_rows, img_cols)
else:
    x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
    x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
    input_shape = (img_rows, img_cols, 1)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

model = Sequential()
model.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu',
                 input_shape=input_shape))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation='softmax'))

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adadelta(),
              metrics=['accuracy'])

model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test, y_test))
score = model.evaluate(x_test, y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

activations = dict()
model_2 = Sequential()
for i in range(len(model.layers)):
    layer_info = model.layers[i].get_config()
    if layer_info['name'].find('dense') >= 0:
        if i == len(model.layers)-1:
            layer_name = 'output'
        else:
            layer_name = layer_info['name']
        num_units = layer_info['units']
        activation_fn = layer_info['activation']
        layer_weights = model.layers[i].get_weights()
        if i == 0:
            model_2.add(Dense(num_units, activation=activation_fn,
                input_shape=(input_dim,), weights=layer_weights))
        elif i < len(model.layers)-1:
            model_2.add(Dense(num_units, activation=activation_fn,
                                            weights=layer_weights))
        else:
            model_2.add(Dense(num_classes, activation='softmax'))
        activations[layer_name] = model_2.predict(x_test)
    elif layer_info['name'].find('conv2d') >= 0:
        kernel_dims = layer_info['kernel_size']
        num_filters = layer_info['filters']
        activation_fn = layer_info['activation']
        stride_dim = layer_info['strides'][0]
        pad_set = layer_info['padding']
        layer_weights = model.layers[i].get_weights()
        if i == 0:
            input_dims = layer_info['batch_input_shape'][1:]
            model_2.add(Conv2D(num_filters, kernel_size=kernel_dims,
                activation=activation_fn, input_shape=input_dims,
                strides=stride_dim, padding=pad_set, weights=layer_weights))
        else:
            model_2.add(Conv2D(num_filters, kernel_size=kernel_dims,
                activation=activation_fn, strides=stride_dim, padding=pad_set,
                weights=layer_weights))
    elif layer_info['name'].find('flatten') >= 0:
        model_2.add(Flatten())
    elif layer_info['name'].find('max_pooling') >= 0:
        pool_dims = layer_info['pool_size']
        stride_dim = layer_info['strides'][0]
        model_2.add(MaxPooling2D(pool_size=pool_dims, strides=stride_dim))
pdb.set_trace()
