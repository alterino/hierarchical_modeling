'''Trains a simple deep NN on the MNIST dataset.
Gets to 98.40% test accuracy after 20 epochs
(there is *a lot* of margin for parameter tuning).
2 seconds per epoch on a K520 GPU.

source: https://github.com/keras-team/keras/blob/master/examples/mnist_mlp.py
'''

from __future__ import print_function

import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import RMSprop

import pdb

batch_size = 128
num_classes = 10
epochs = 10
input_dim = 784

# the data, split between train and test sets
(x_train, y_train), (x_test, y_test) = mnist.load_data()

x_train = x_train.reshape(60000, 784)
x_test = x_test.reshape(10000, 784)
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

model = Sequential()
model.add(Dense(8, activation='relu', input_shape=(784,)))
model.add(Dropout(0.2))
model.add(Dense(16, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(num_classes, activation='softmax'))

model.summary()

model.compile(loss='categorical_crossentropy',
              optimizer=RMSprop(),
              metrics=['accuracy'])

history = model.fit(x_train, y_train,
                    batch_size=batch_size,
                    epochs=epochs,
                    verbose=1,
                    validation_data=(x_test, y_test))
score = model.evaluate(x_test, y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

activations = dict()
model_2 = Sequential()
for i in range(len(model.layers)):
    layer_info = model.layers[i].get_config()
    if layer_info['name'].find('dense') >= 0:
        if i == len(model.layers)-1:
            layer_name = 'output'
        else:
            layer_name = layer_info['name']
        num_units = layer_info['units']
        activation_fn = layer_info['activation']
        layer_weights = model.layers[i].get_weights()
        if i == 0:
            model_2.add(Dense(num_units, activation=activation_fn,
                input_shape=(input_dim,), weights=layer_weights))
        elif i < len(model.layers)-1:
            model_2.add(Dense(num_units, activation=activation_fn,
                                            weights=layer_weights))
        else:
            model_2.add(Dense(num_classes, activation='softmax'))
        activations[layer_name] = model_2.predict(x_test)
pdb.set_trace()
