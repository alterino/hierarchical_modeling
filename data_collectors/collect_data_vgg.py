from keras.applications.vgg16 import VGG16
from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input
from keras.applications.vgg16 import decode_predictions
from keras import backend
from shutil import copyfile

import numpy as np
import pickle
import datetime
import os
import json
import pdb

def search(values, target):
    for k in values:
        for v in values[k]:
            if target in v:
                return k
    return None

def save_obj(obj, name):
    with open('obj/'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    with open('obj/' + name + '.pkl', 'rb') as f:
        return pickle.load(f)

def backup_dictfile(name):
    src = 'obj/' + name + '.pkl'
    dst = 'obj/tmp_check/' + name + '.pkl'
    copyfile(src, dst)

name = 'vgg16_data'
vgg16_datadict = dict()

class_index_filepath = os.path.expanduser('~/.keras/models/imagenet_class_index.json')
data_dir = os.path.expanduser('~/data/imagenet-data/raw-data/validation/')
model = VGG16(weights='imagenet')
inp = model.input
outputs = [layer.output for layer in model.layers]
functor = backend.function([inp, backend.learning_phase()], outputs)

with open("data_log.txt", "w+") as log_f:
    log_f.write('Processing run started %s' % datetime.datetime.now())

with open(class_index_filepath) as f:
    class_index_dict = json.load(f)

for root, dirs, _  in os.walk(data_dir):
    for directory in dirs:
        class_key = search(class_index_dict, directory)
        temp_data_dict = dict()
        for class_dir,_,files in os.walk(os.path.join(root, directory)):
            idx = 0
            fc1_data = np.zeros(shape=(len(files), 4096))
            fc2_data = np.zeros(shape=(len(files), 4096))
            out_data = np.zeros(shape=(len(files), 1000))
            for filename in files:
                img_path = os.path.join(class_dir, filename)
                img = image.load_img(img_path, target_size=(224, 224))
                x = image.img_to_array(img)
                x = np.expand_dims(x, axis=0)
                x = preprocess_input(x)
                pdb.set_trace()
                layer_outs = functor([x, 0.])
                fc1_data[idx]=layer_outs[-3]
                fc2_data[idx]=layer_outs[-2]
                out_data[idx]=layer_outs[-1]
                idx = idx + 1
            temp_data_dict['fc1'] = fc1_data
            temp_data_dict['fc2'] = fc2_data
            temp_data_dict['output'] = out_data
            vgg16_datadict[class_key] = temp_data_dict
            save_obj(vgg16_datadict, name)
            backup_dictfile(name)
            with open("data_log.txt", "a+") as log_f:
                log_f.write('\n%s:completed at %s' % (directory,datetime.datetime.now()))

