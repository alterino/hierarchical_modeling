import numpy as np
from data_processing import conceptors as C
import pickle
import os
import pdb
import json
import time
import datetime

DATA_DIRECTORY = os.path.expanduser('~/data/conceptors/')

# -------------------------------------------------------------------#
def search(values, target):
    for k in values:
        for v in values[k]:
            if target in v:
                return k
    return None

def save_obj(obj, name):
    with open(DATA_DIRECTORY + name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    with open(DATA_DIRECTORY + name + '.pkl', 'rb') as f:
        return pickle.load(f)
# -------------------------------------------------------------------#


with open("data_log_4.txt", "w+") as log_f:
    log_f.write('Processing run started %s' % datetime.datetime.now())

data_filepath = 'inceptionV3_data'
data_dict = load_obj(data_filepath)

class_index_filepath = os.path.expanduser('~/.keras/models/imagenet_class_index.json')

with open(class_index_filepath) as f:
    class_index_dict = json.load(f)

for class_index in class_index_dict:
    if class_index in data_dict:
        activations = data_dict[class_index]
        R = []
        for layer in activations:
            temp_R = C.calc_R(layer)
            R.append(temp_R)
        filename = 'correlation_matrices/inceptionV3/R_synset_' + class_index.zfill(3)
        save_obj(R, filename)
        with open("data_log_4.txt", "a+") as log_f:
            log_f.write('file for synset %s written.\n' % class_index )
