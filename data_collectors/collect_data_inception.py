from tensorflow.keras.applications.inception_v3 import InceptionV3
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.inception_v3 import preprocess_input
from tensorflow.keras.applications.inception_v3 import decode_predictions
from tensorflow.keras.layers import Input
from tensorflow.keras import backend
from shutil import copyfile
from skimage.measure import block_reduce

import numpy as np
import pickle
import datetime
import os
import json
import pdb

def search(values, target):
    for k in values:
        for v in values[k]:
            if target in v:
                return k
    return None

def save_obj(obj, name):
    with open('obj/'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    with open('obj/' + name + '.pkl', 'rb') as f:
        return pickle.load(f)

def backup_dictfile(name):
    src = 'obj/' + name + '.pkl'
    dst = 'obj/tmp_check/' + name + '.pkl'
    copyfile(src, dst)

name = 'inceptionV3_data'
inceptionV3_datadict = dict()

inp = Input(shape=(224, 224, 3))

class_index_filepath = os.path.expanduser('~/.keras/models/imagenet_class_index.json')
data_dir = os.path.expanduser('~/data/imagenet-data/raw-data/validation/')
model = InceptionV3(input_tensor=inp, weights='imagenet')
outputs = [layer.output for layer in model.layers]
pdb.set_trace()
functor = backend.function([inp, backend.learning_phase()], outputs)

with open("data_log_2.txt", "w+") as log_f:
    log_f.write('Processing run started %s' % datetime.datetime.now())

with open(class_index_filepath) as f:
    class_index_dict = json.load(f)

layer_names = []
layer_dims = []
for layer in model.layers:
    if 'activation' in layer.name:
        layer_names.append(layer.name)
        layer_dims.append(layer.output_shape)

inceptionV3_datadict['layer_names'] = layer_names
inceptionV3_datadict['layer_dims'] = layer_dims

for root, dirs, _  in os.walk(data_dir):
    for directory in dirs:
        class_key = search(class_index_dict, directory)
        temp_data = []
        for class_dir,_,files in os.walk(os.path.join(root, directory)):
            activation_cnt = 0
            for layer in model.layers:
                if 'activation' in layer.name:
                    channel_depth = layer.output_shape[-1]
                    temp_data.append(np.empty((len(files), channel_depth), dtype=np.float32))
            for file_count, filename in enumerate(files):
                img_path = os.path.join(class_dir, filename)
                img = image.load_img(img_path, target_size=(224, 224))
                x = image.img_to_array(img)
                x = np.expand_dims(x, axis=0)
                x = preprocess_input(x)
                layer_outs = functor([x, 0.])
                activation_count = 0
                for i, layer in enumerate(model.layers):
                    if 'activation' in layer.name:
                        temp_outs = layer_outs[i]
                        out_shape = temp_outs.shape
                        temp_outs = block_reduce(temp_outs, 
                                             block_size=(1, out_shape[1], out_shape[2], 1), 
                                             func=np.mean)
                        temp_data[activation_count][file_count] = temp_outs
                        activation_count += 1
            inceptionV3_datadict[class_key] = temp_data
            pdb.set_trace()
            save_obj(inceptionV3_datadict, name)
            backup_dictfile(name)
            with open("data_log_2.txt", "a+") as log_f:
                log_f.write('\n%s:completed at %s' % (directory,datetime.datetime.now()))

