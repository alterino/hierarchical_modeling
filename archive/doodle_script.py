import pdb
import numpy as np
import os
import operator as op
from functools import reduce
from data_processing.utils import calculate_depth
import json
import matplotlib.pyplot as plt

from nltk.corpus import wordnet as wn

#
def write_file_for_stuff(key_list_by_matrix_index):
    with open('tmp/.key_list.txt', 'w') as f:
        for item in key_list_by_matrix_index:
            f.write('%s\n' % item)

def ncr(n,r):
    r = min(r, n-r)
    numer = reduce(op.mul, range(n, n-r, -1), 1)
    denom = reduce(op.mul, range(1, r+1), 1)
    return numer / denom


if __name__ == '__main__':
    data_dir = os.path.expanduser('~/data/conceptors/analysis/resnet50/')
    tot_cnt = 500
    for i in range(4):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        cnt = 0
        plt_cnt = 0
    for filename in os.listdir(data_dir):
        print(filename)
        with open(data_dir + filename) as f:
            content = f.readlines()
        content = [x.strip() for x in content]
        x = []
        y = []
        for ln in content:
            str_split = ln.split(':')
            x.append(int(str_split[0]))
            y.append(float(str_split[1]))
        ax.plot(x, y)
        cnt += 1
        print(cnt)
        if cnt >= 50 and plt_cnt <= 3:
            plt.grid()
            plt.xlabel('network depth')
            plt.ylabel('similarity')
            plt.show()
            fig.savefig('plot_' + str(plt_cnt) + '.png')
            plt.close(fig)
            plt_cnt += 1
            fig = plt.figure()
            ax = fig.add_subplot(111)
            cnt = 0
        elif cnt >= 50 and plt_cnt > 3:
            break
    plt.show()
    pdb.set_trace()
