import time
from keras.callbacks import Callback

class AutoRestart(Callback):
    '''Restart the script if walltime might be reached in the next epoch
    '''
    def __init__(self, filepath, start_time, verbose=0, walltime=0):
        super(AutoRestart, self).__init__()

        self.filepath = filepath
        self.start_time = start_time
        self.walltime = walltime
        self.wait = 0
        self.verbose = verbose
        self.epoch_start = 0
        self.epoch_average = 0
        self.stopped_epoch = 1
        self.reachedWalltime = False

    def on_train_begin(self, logs={}):
        if self.verbose > 0:
            print('qsubRestart Callback')

    def on_epoch_begin(self, epoch, logs={}):
        self.epoch_start=time.time()

    def on_epoch_end(self, epoch, logs={}):
        self.model.save_weights(self.filepath+str(epoch)+'.hdf5', overwrite=True)
        epochtime = (time.time() - self.epoch_start)
        if self.epoch_average > 0:
            self.epoch_average = (self.epoch_average + epochtime) / 2
        else:
            self.epoch_average = epochtime
        if (time.time() - self.start_time + 1.2*self.epoch_average)>self.walltime:
            print("will run over walltime: %s s" % int(time.time() - self.start_time + 3*self.epoch_average))
            self.reachedWalltime = True
            self.stopped_epoch = epoch
            self.model.stop_training = True
        #else:
            #print("walltime in: %s s" % int(self.walltime - (time.time() - self.start_time)))

        if self.verbose > 0:
            print("-Runtime: %s s, Epoch runtime %s s, Average Epoch runtime %s s ---" % (int((time.time() - self.start_time)), int(epochtime) , int(self.epoch_average)) )


    def on_train_end(self, logs={}):
        epochtime = (time.time() - self.epoch_start)
        if self.verbose > 0:
            print("-Total Runtime: %s s, Epoch runtime %s s, Average Epoch runtime %s s---" % (int((time.time() - self.start_time)), int(epochtime) , int(self.epoch_average)) )
