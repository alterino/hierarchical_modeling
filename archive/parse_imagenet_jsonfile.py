import json
from nltk.corpus import wordnet as wn

import pdb

# documentation for nltk library can be found at
# https://www.nltk.org/api/nltk.corpus.reader.html#module-nltk.corpus.reader.wordnet


# returns a dictionary with keys corresponding to class imagenet IDs (value from 0-999) and
# values giving the corresponding wnid for that class (format {part-of-speech}{offset}) as well
# as the wordnet semantic label, as a tuple
# {part-of-speech} is always 'n' for imagenet classes since they are all nouns nouns
def load_imagenet_wnid_dict(filepath='imagenet_class_index.json'):
    with open(filepath) as f:
        imagenet_wnid_dict = json.load(f)
    
    return imagenet_wnid_dict


if __name__ == '__main__':
    imagenet_wnid_dict = load_imagenet_wnid_dict()

    for k in imagenet_wnid_dict.keys():
        imagenet_id = int(k)
        wordnet_id = imagenet_wnid_dict[k][0]
        semantic_label = imagenet_wnid_dict[k][1]
        print('inid: %.03i, wnid: %s, semantic_label: %s' % (imagenet_id, 
                                                            wordnet_id,
                                                            semantic_label))
        # this is how you use the wordnet id to get the "synset" object for the class
        synset = wn._synset_from_pos_and_offset(wordnet_id[0], int(wordnet_id[1:]))
        child_definition = synset.definition()

        # nltk.corpus methods operate on synset object
        superclass_synsets = synset.hypernyms()
        superclass_distances = synset.hypernym_distances()

        for parent_synset in superclass_synsets:
            parent = parent_synset.name()
            parent_definition = parent_synset.definition()
            print('%s is a member of the %s class' % (semantic_label, parent))
            print('%s is defined as a \'%s\'' % (semantic_label, child_definition))
            print('%s is defined as a \'%s\'' % (parent, parent_definition))

        print('--All superclasses of %s with corresponding hypernym distance--' % semantic_label)
        print(superclass_distances)
        print('\n')

    
