from keras.applications.vgg16 import VGG16
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from keras.applications.vgg16 import preprocess_input
from keras.applications.vgg16 import decode_predictions
from keras import backend
from keras.applications.inception_v3 import InceptionV3
from keras.models import Model, Sequential
from keras.layers import Dense, Flatten, Input
from keras import backend as K
from keras import optimizers

from keras.callbacks import EarlyStopping, CSVLogger, Callback, TensorBoard
from qsubRestart import AutoRestart

import numpy as np
import datetime
import pickle
import time
import os
import pdb
import fnmatch
import argparse
from data_processing import utils

parser = argparse.ArgumentParser()
parser.add_argument('checkpoint_dir', type=str, help='Output dir for checkpoints')

args = parser.parse_args()

train_dir = os.path.expanduser('~/data/imagenet-data/raw-data/train')
train_subdirs = [train_dir + '/' + s for s in os.listdir(train_dir)]
validation_dir = os.path.expanduser('~/data/imagenet-data/raw-data/validation')
val_subdirs = [validation_dir + '/' + s for s in os.listdir(train_dir)]
n_training_samples = sum([len(fnmatch.filter(os.listdir(subdir),'*.JPEG')) for subdir in train_subdirs])
n_validation_samples = sum([len(fnmatch.filter(os.listdir(subdir),'*.JPEG')) for subdir in val_subdirs])

checkpoints_path = args.checkpoint_dir
objectdump_path = checkpoints_path + 'tmp/'

inp_shape = (224,224,3)
batch_size=64
nb_classes=1000
top_epochs=10

try:
    walltimeString = str(os.environ['WALL'])
    sepoch = int(os.environ['ST_EPOCH'])
    grid_bool=True
except:
    sepoch = 0
    grid_bool=False

try:
    job_id = os.environ['JOB_ID']
except:
    job_id = 9999999

logdir = 'logs/' + job_id

if os.path.isdir(logdir):
    print('warning: log directory already exists')
else:
    os.mkdir(logdir)
tensorbard_cbk = TensorBoard(log_dir=logdir)


def get_sec(time_str):
    h, m, s = time_str.split(':')
    return int(h) * 3600 + int(m) * 60 + int(s)

if grid_bool:
    walltime = get_sec(walltimeString)
    start_time = time.time()

if __name__ == '__main__':
    class_dict = utils.get_default_class_dict()

    inp_tensor = Input(shape=inp_shape)
    
    # create the base pre-trained model
    base_model = VGG16(weights='imagenet', include_top=False, input_tensor=inp_tensor)

    new_model = Sequential()
    for layer in base_model.layers:
        new_model.add(layer)

    new_model.add(Flatten())

    # add a global spatial average pooling layer
    x = new_model.output
    # let's add a fully-connected layer
    x = Dense(4096, activation='relu')(x)
    x = Dense(4096, activation='relu')(x)
    # and a logistic layer -- let's say we have 200 classes
    predictions = Dense(1000, activation='softmax')(x)

    # this is the model we will train
    model = Model(inputs=new_model.input, outputs=predictions)
#   train_req, test_req, n_weights, n_vals = utils.calculate_memory_requirements(model.layers, batch_size=32)
#   print('training memory required: %.2f' % train_req)

    train_datagen = ImageDataGenerator(rescale=1./255,
                                       rotation_range=20,
                                       width_shift_range=0.2,
                                       height_shift_range=0.2,
                                       horizontal_flip=True,
                                       fill_mode='nearest')
    valid_datagen = ImageDataGenerator(rescale=1./255)

    validation_generator = valid_datagen.flow_from_directory(validation_dir,
                                                             target_size=inp_shape[:2],
                                                             batch_size=batch_size,
                                                             class_mode='categorical')

    # first: train only the top layers (which were randomly initialized)
    # i.e. freeze all convolutional InceptionV3 layers
    for layer in base_model.layers:
        layer.trainable = False

    # compile the model (should be done *after* setting layers to non-trainable)
    model.compile(optimizer=optimizers.Adam(), 
                  loss='categorical_crossentropy', 
                  metrics=['accuracy'])

    model.summary()

    if grid_bool:
        if sepoch > 0:
            model.load_weights(checkpoints_path + '/checkpoint_epoch'+str(sepoch)+'.hdf5')
    
        if sepoch > 0:
                csv_logger = CSVLogger('logfile.log', append=True)
        else:
                csv_logger = CSVLogger('logfile.log')
    
        auto_restart = AutoRestart(filepath=checkpoints_path + '/checkpoint_epoch',
                                   start_time=start_time,
                                   verbose = 0,
                                   walltime=walltime)
    
        early_stopping = EarlyStopping(monitor='val_loss', patience=500)

    print('fitting for hierarchy...')
    init_multiplier = 10

    # train the model on the new data for a few epochs
    for curr_epoch in range(sepoch, top_epochs):
        multiplier = init_multiplier + curr_epoch
        target_vectors = utils.calculate_target_vectors(class_dict, 
                                                        true_multiplier=multiplier, 
                                                        metric='resnik')
        train_generator = train_datagen.flow_from_directory(train_dir,
                                                            target_size=inp_shape[:2],
                                                            batch_size=batch_size,
                                                            labels=target_vectors,
                                                            class_mode='raw')
        print('epoch = %i, multiplier = %.2f' % (curr_epoch,multiplier))
        
        model.fit_generator(train_generator,
                            epochs=curr_epoch+1,
                            steps_per_epoch=n_training_samples // batch_size,
                            validation_data=validation_generator,
                            validation_steps=n_validation_samples // batch_size,
                            verbose=1,
                            callbacks=[auto_restart, early_stopping, csv_logger],
                            initial_epoch=curr_epoch)
        
    train_generator = train_datagen.flow_from_directory(train_dir,
                                                        target_size=inp_shape[:2],
                                                        batch_size=batch_size,
                                                        class_mode='categorical')
    print('epoch = %i, fitting with true label' % top_epochs)
    model.fit_generator(train_generator,
                        epochs=top_epochs+3,
                        steps_per_epoch=n_training_samples // batch_size,
                        validation_data=validation_generator,
                        validation_steps=n_validation_samples // batch_size,
                        verbose=1,
                        callbacks=[auto_restart, early_stopping, csv_logger],
                        initial_epoch=top_epochs)

    if grid_bool:
        if auto_restart.reachedWalltime == 1:
            from subprocess import call
            recallParameter = ('qsub -v WALL=' + str(walltimeString) + ',ST_EPOCH=' + 
                                str(auto_restart.stopped_epoch) + 
                                ' training/train_model.sge ')
            call(recallParameter, shell=True)
        else:
            score = model.evaluate(x_vali, y_vali, verbose=0 , batch_size=batchSize)
            print('Test loss:', score[0])
            print('Test accuracy:', score[1])

    # at this point, the top layers are well trained and we can start fine-tuning
    # convolutional layers from inception V3. We will freeze the bottom N layers
    # and train the remaining top layers.

    # let's visualize layer names and layer indices to see how many layers
    # we should freeze:
#   for i, layer in enumerate(base_model.layers):
#              print(i, layer.name)

#   # we chose to train the top 2 inception blocks, i.e. we will freeze
#   # the first 249 layers and unfreeze the rest:
#   for layer in model.layers[:249]:
#       layer.trainable = False
#   for layer in model.layers[249:]:
#       layer.trainable = True

#   # we need to recompile the model for these modifications to
#   # take effect
#   # we use SGD with a low learning rate
#   from keras.optimizers import SGD
#   model.compile(optimizer=SGD(lr=0.0001, momentum=0.9),
#                 loss='categorical_crossentropy')

#   # we train our model again (this time fine-tuning the top 2
#   # inception blocks
#   # alongside the top Dense layers
#   model.fit_generator(...)


