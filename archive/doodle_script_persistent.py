import pdb
import os

import data_utils
import pkl_utils

data_dir = '/home/student/m/mimarino/tmp/data/conceptors/analysis/working_data/'

for root, _, filenames in os.walk(data_dir):
    for i in range(len(filenames)):
        for j in range(i):
            sim_val = data_utils.calculate_similarity((filenames[i], filenames[j]),
                                                      write_tmp_files=True,
                                                      root_dir=data_dir)

pdb.set_trace()
