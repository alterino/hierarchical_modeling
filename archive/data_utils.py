import os
import pdb
import numpy as np
import conceptors as C

import json
import time

from pkl_utils import save_obj
from pkl_utils import load_obj

root_path = os.path.expanduser('~/tmp/data/conceptors/analysis/')
data_dirpath = root_path + 'working_data/'
out_filepath = root_path + 'similarity_matrices.pkl'

class_index_filepath = os.path.expanduser('~/.keras/models/imagenet_class_index.json')

def calculate_similarity(filenames=None,
                         key_list={'fc1', 'fc2'},
                         root_dir=data_dirpath,
                         subset_idx=None,
                         write_tmp_files=False,
                         out_dir = None):
    if filenames is None:
        raise ValueError('input data must be specified by filenames list')
#   with open(class_index_filepath) as f:
#       class_index_dict = json.load(f)
    if subset is None:
        files_idx_toprocess = range(len(filenames))
    else:
        files_idx_toprocess = subset_idx
    idx = 0
    cos_matrices = dict()
    key_list_by_matrix_index = [''] * len(filenames)
    key_list_filestr = 'tmp/key_list_by_matrix_index.txt'
    key_file = open(key_list_filstr, 'w')
    for k in key_list:
        cos_matrices[k] = np.zeros( [len(filenames), len(filenames)] )
    for i in files_idx_toprocess:
        key_list_by_matrix_index[i] = [str(int(s)) for s in filenames[i][:-4].split('_') if s.isdigit()][-1]
        dict_class1 = load_obj( root_dir + filenames[i] )
        for j in range(i, len(filenames)):
            print('i=' + str(i) + ', j=' + str(j))
            print(key_list)
            for k in key_list:
                dict_class2 = load_obj( root_dir + filenames[j] )
                for k in key_list:
                    if i == j:
                        cos_matrices[k][i][j] = 1
                    else:
                        try:
                            cos_matrices[k][i][j] = C.generalized_cosine(dict_class1[k], dict_class2[k])
                        except:
                            pdb.set_trace()
                        cos_matrices[k][j][i] = cos_matrices[k][i][j]
            if write_tmp_files:
                idxs_str = str(i).zfill(3) + '_' + str(j).zfill(3)
                temp_filestr = 'tmp/' + idxs_str + '.tmp'
                temp_file = open(temp_filestr, 'w')
                pdb.set_trace()
                for k in cos_matrices:
                    temp_file.write(k + ':' + str(cos_matrices[k][i][j]) + '\n')

    return cos_matrices

#if __name__='__main__':

