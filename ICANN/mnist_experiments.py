# USAGE
# python run_mnist_training.py

# import the necessary packages
from models import GenericNet
#from models import not_ShallowNet
from tensorflow.keras.callbacks import LearningRateScheduler, ModelCheckpoint
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.datasets import mnist
from tensorflow.compat.v1.keras.backend import set_session
from tensorflow.compat.v1 import ConfigProto, Session
from sklearn.preprocessing import LabelBinarizer
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from tensorflow.keras import backend as K
from tensorflow import device
import matplotlib.pyplot as plt
import numpy as np
import argparse
import os

import pdb
from pathlib import Path
from datetime import datetime
import pickle

now = datetime.now()

# to fix cudnn handle error
config = ConfigProto()
config.gpu_options.allow_growth = True
sess = Session(config=config)
set_session(sess)

ap = argparse.ArgumentParser()
ap.add_argument("-p", "--plot", type=str, default="plot.png",
        help="path to output plot file")
ap.add_argument("-d", "--outdir", type=str, default="checkpoints/")
ap.add_argument("-g", "--gpu", type=int, default=0)
args = vars(ap.parse_args())

if args['outdir'][-1] is not '/':
    args['outdir'] = args['outdir'] + '/'

# define the total number of epochs to train for initial learning
# rate, and batch size
NUM_EPOCHS = 100
INIT_LR = 0.01
BATCH_SIZE = 128

input_shape = (28, 28, 1)
classes = 10 

model_params = dict()
model_params['conv_depths'] = [16, 32]
model_params['fc_depths'] = [16, 16, 32]
model_params['conv_pool'] = [False, True]

time_str = now.strftime("%y%m%d_%H%M")
args['outdir'] = args['outdir'] + time_str + '/'
args['plot'] = args['outdir'] + args['plot']
Path(args['outdir']).mkdir(parents=True)

save_vars = (model_params, time_str)

with open(args['outdir'] + 'model_params.pkl', 'wb') as f:
    pickle.dump(save_vars, f)

with device('/gpu:' + str(args["gpu"])):
    print("[INFO] accessing MNIST...")
    ((trainData, trainLabels), (testData, testLabels)) = mnist.load_data()
    
    if K.image_data_format() == "channels_first":
    	trainData = trainData.reshape((trainData.shape[0], 1, 28, 28))
    	testData = testData.reshape((testData.shape[0], 1, 28, 28))
    
    else:
    	trainData = trainData.reshape((trainData.shape[0], 28, 28, 1))
    	testData = testData.reshape((testData.shape[0], 28, 28, 1))
    
    trainData = trainData.astype("float32") / 255.0
    testData = testData.astype("float32") / 255.0
    
    le = LabelBinarizer()
    trainLabels = le.fit_transform(trainLabels)
    trainLabels = trainLabels.astype("float")
    testLabels = le.transform(testLabels)
    
    opt = SGD(lr=INIT_LR)
#   model = not_ShallowNet.build(width=28, height=28, depth=1, classes=10)
    model = GenericNet.build(*input_shape, classes, **model_params)
    model.compile(loss="categorical_crossentropy", optimizer=opt,
    	metrics=["accuracy"])

    
    #schedule = PolynomialDecay(maxEpochs=NUM_EPOCHS, initAlpha=INIT_LR,
    #        power=1.0)
    fname = os.path.sep.join([args["outdir"], 
                "weights-genericnet-{epoch:03d}-{val_loss:.4f}.hdf5"])
    
    #callbacks = [LearningRateScheduler(schedule), ModelCheckpoint(fname)]
    callbacks = [ModelCheckpoint(fname,  monitor='val_loss', save_freq='epoch')]
    
    # train the network
    print("[INFO] training network...")
    H = model.fit(trainData, trainLabels,
    	          validation_data=(testData, testLabels), 
                  batch_size=BATCH_SIZE,
    	          epochs=NUM_EPOCHS, 
                  callbacks=callbacks,
                  verbose=1)
    # evaluate the network
    print("[INFO] evaluating network...")
    predictions = model.predict(testData, batch_size=128)
    print(classification_report(testLabels.argmax(axis=1),
    	                        predictions.argmax(axis=1),
	                        target_names=[str(x) for x in le.classes_]))

# plot the training loss and accuracy
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, NUM_EPOCHS), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, NUM_EPOCHS), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, NUM_EPOCHS), H.history["accuracy"], label="train_acc")
plt.plot(np.arange(0, NUM_EPOCHS), H.history["val_accuracy"], label="val_acc")
plt.title("Training Loss and Accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend()
plt.savefig(args["plot"])
# plt.show()
