import os
import sys
import pdb
import re
import h5py
import numpy as np
import json
import matplotlib.pyplot as plt
import sklearn.cluster as cluster

from nltk.corpus import wordnet as wn
from nltk.corpus import wordnet_ic

def generate_random_distmat(dims):
    M = np.random.random_sample(dims)
    M = (M + M.T)/2
    for i in range(len(M)):
        M[i,i] = 0
    return M
    
def cluster_spectral(D, K, affinity_param='precomputed', assign_labels_param='discretize'):
    spectral = cluster.SpectralClustering(n_clusters=K,
                                          eigen_solver='arpack',
                                          affinity=affinity_param,
                                          assign_labels=assign_labels_param)
    spectral.fit(D)
    grp = spectral.fit_predict(D) + 1
    return grp

def cluster_hierarchical(D, method='average'):
    D = squareform(D)
    clustering = linkage(D, method=method)
    return clustering

# parses linkage returned from the scipy.cluster.hierarchy.linkage() function and returns a
# dictionary with keys corresponding to the reference index for that cluster (using index naming
# convention of the linkage() function). If return_labels is set to True, an additional
# dictionary entry corresponding to the semantic labels of all group members is returned. In this
# case, a class_dict must be passed with keys corresponding to the base class index and values
# corresponding to the base object class semantic labels.
def parse_linkage(linkage, return_labels=False, class_dict=None):
    if return_labels is True:
        assert (isinstance(class_dict, dict)), "must provide class_dict for semantic labels"
    num_leaves = len(linkage)+1
    hierarchy_dict = dict()
    for i in range(2*len(linkage)+1):
        k = str(i)
        hierarchy_dict[k] = dict()
        hierarchy_dict[k]['parent'] = -1
        hierarchy_dict[k]['leaves'] = []
        hierarchy_dict[k]['children'] = -1
        hierarchy_dict[k]['dist'] = -1
        hierarchy_dict[k]['member_count'] = -1
        hierarchy_dict[k]['node_depth'] = -1
    for i in range(len(linkage)):
        group_idx = num_leaves + i
        tmp_dict = dict()
        child_idxs = [int(linkage[i,0]), int(linkage[i,1])]
        tmp_dict['children'] = child_idxs
        tmp_dict['dist'] = linkage[i,2]
        tmp_dict['member_count'] = linkage[i,3]
        tmp_dict['leaves'] = []
        for idx in child_idxs:
            if idx < 1000:
                tmp_dict['leaves'].append(idx)
                hierarchy_dict[str(idx)]['parent'] = group_idx
                hierarchy_dict[str(idx)]['leaves'] = None
                hierarchy_dict[str(idx)]['children'] = None
                hierarchy_dict[str(idx)]['dist'] = linkage[i,2]
                hierarchy_dict[str(idx)]['member_count'] = 1
            else:
                hierarchy_dict[str(idx)]['parent'] = group_idx
                leaf_list = expand_leaves(str(idx), hierarchy_dict)
                for leaf in leaf_list:
                    tmp_dict['leaves'].append(leaf)
        group_key = str(group_idx)
        for k in tmp_dict.keys():
            hierarchy_dict[group_key][k] = tmp_dict[k]
    
    hierarchy_dict = calculate_depth(hierarchy_dict, num_leaves+len(linkage)-1, num_leaves)

    if return_labels is True:
        for k in hierarchy_dict.keys():
            if int(k) < 1000:
                inid_list = [k]
            else:
                inid_list = hierarchy_dict[k]['leaves']
            label_list = inid_to_semantic_label(inid_list, class_dict)
            hierarchy_dict[k]['semantic_labels'] = label_list

    return hierarchy_dict

def expand_leaves(key, hierarchy_dict):
    if key not in hierarchy_dict.keys():
        raise KeyError('key not found in hierarchy_dict')
    return hierarchy_dict[key]['leaves']

def calculate_depth(hierarchy_dict, start_node, num_leaves, parent_depth=-1):
    node_depth = parent_depth + 1
    hierarchy_dict[str(start_node)]['node_depth'] = node_depth
    
    if start_node > num_leaves-1:
        child_idxs = hierarchy_dict[str(start_node)]['children']
        for idx in child_idxs:
            hierarchy_dict = calculate_depth(hierarchy_dict, idx, num_leaves, node_depth)
    return hierarchy_dict

def inid_to_semantic_label(inid_list, class_idx_dict):
    label_list = list()
    for i in range(len(inid_list)):
        if not isinstance(inid_list[i], str):
            inid_list[i] = str(inid_list[i])
        label_list.append(class_idx_dict[inid_list[i]][1])
    return label_list

# depths should be a 2d tuple of the form (depth(A), depth(B))
# depths no longer important since we are just doing f1 score
def class_similarity(A, B, get_accuracy=False):
    if A is not set:
        A = set(A)
    if B is not set:
        B = set(B)

    if len(A&B) == 0:
        sim_AB = 0
        recall = 0
        precision = 0
    else:
        recall = len(A&B)/len(B)
        precision = len(A&B)/len(A)
        sim_AB = 2*recall*precision/(recall + precision)

    if get_accuracy:
        return (sim_AB, recall, precision)
    else:
        return sim_AB

def get_accuracy_results(A, B):
    if A is not set:
        A = set(A)
    if B is not set:
        B = set(B)

    recall = len(A&B)/len(B)
    precision = len(A&B)/len(A)

    return recall, precision

# parses directory with one .txt file for each entry in a matrix assuming a filename according
# ROW_COL.txt where ROW and COL are the row and column indices for those values. Matrix is
# assumed to be a symmetric with diagonal set to 1.  Text files should be of the form key:val\n
# for each value, so number of matrices in returned dictionary will be defined by number of
# lines in the text files.  
# path : directory where .txt files exist 
# keys : list of keys referring to matrices, should be first column of .txt file 
# n : dimension of square matrix

def parse_tmp_files(path, keys, n):

    num_leaves = n
    data_dir = path
    sim_mats = dict()
    for root, _, filenames in os.walk(data_dir):
        for k in keys:
            sim_mats[k] = np.zeros((num_leaves, num_leaves))
            for i in range(num_leaves):
                sim_mats[k][i][i] = 1
        for fn in filenames:
            fn_parsed = re.split('[_.,]', fn)
            try:
                idx1 = int(fn_parsed[0])
                idx2 = int(fn_parsed[1])
            except:
                raise ValueError("text files must be of the form ROW_COL.txt")
            with open(root + fn) as f:
                for line in f:
                    line_parsed = re.split(':', line)
                    key = line_parsed[0]
                    if not key in sim_mats.keys():
                        raise ValueError("key from text file does not match keys provided")
                    val = float(line_parsed[1].replace('\n', ''))
                    sim_mats[key][idx1][idx2] = val
                    sim_mats[key][idx2][idx1] = val
    return sim_mats

def calculate_target_vectors(class_dict, metric='path', true_multiplier=10, normalize=True, **kwargs):
    ids = list(class_dict.keys())
    target_vectors = [None]*len(ids)
    if metric == 'resnik' and 'ic' not in kwargs:
        # used as default information content if not passed by user
        kwargs['ic'] = wordnet_ic.ic('ic-brown.dat')
    for a in ids:
        wnid = class_dict[a][0]
        synset_A = wn._synset_from_pos_and_offset(wnid[0], int(wnid[1:]))
        tmp_vec = np.zeros(len(ids), dtype=np.float64) - 1
        for b in ids:
            if int(a) == int(b) and normalize is True:
                tmp_vec[int(b)] = 0
            else:
                wnid = class_dict[b][0]
                synset_B = wn._synset_from_pos_and_offset(wnid[0], int(wnid[1:]))
                tmp_vec[int(b)] = get_synset_similarity(synset_A, synset_B, metric=metric, **kwargs)
#               print('%s - %.3f' % (synset_B.name(), tmp_vec[int(b)]))
        tmp_vec[int(a)] = tmp_vec.max()*true_multiplier
        if normalize is True:
            tmp_vec = tmp_vec/np.sum(tmp_vec)
        for idx,score in enumerate(tmp_vec):
            wnid = class_dict[str(idx)][0]
            synset = wn._synset_from_pos_and_offset(wnid[0], int(wnid[1:]))
#           print('(%i) %s - %.4e' % (idx, synset.name(), score))
#       plt.figure()
#       plt.hist(tmp_vec, bins=50)
#       print('%i - %s' % (int(a),synset_A.name()))
#       print(tmp_vec[int(a)])
#       print(tmp_vec.sum())
#       plt.show()
#       pdb.set_trace()
        target_vectors[int(a)] = tmp_vec
    return target_vectors

def get_synset_similarity(syn_A, syn_B, metric='path', **kwargs):
    if metric == 'path':
        return syn_A.path_similarity(syn_B)
    elif metric == 'resnik':
        # information content object is required for resnik similarity,
        # defaults to IC based on brown corpus
        if kwargs == {}:
            ic = wordnet_ic.ic('ic-brown.dat')
        elif 'ic' not in kwargs:
            raise KeyError('for resnik similarity the keyword variable \'ic\' must be set')
        else:
            ic = kwargs['ic']
        return syn_A.res_similarity(syn_B, ic)
    elif metric == 'wu-palmer':
        return syn_A.wup_similarity(syn_B)
    else:
        raise ValueError('invalid metric name')


def extract_base_hierarchy(label_dict, ontology='wn'):
    sc_membership = dict()
    sc_list = list()
    if ontology is 'wn':
        for k in label_dict.keys():
            wnid = label_dict[k][0]
            label = label_dict[k][1]
            tmp_synset = wn._synset_from_pos_and_offset('n', int(wnid[1:]))
            if len(tmp_synset.hypernyms()) > 0:
                superclass_list = tmp_synset.hypernyms()
                for sc in superclass_list:
                    offset = sc.offset()
                    sc_wnid = "n{:08d}".format(offset)
                    if sc_wnid not in sc_membership.keys():
                        sc_list.append(sc_wnid)
                        sc_membership[sc_wnid] = list()
                    sc_membership[sc_wnid].append(wnid)
        while len(sc_list) > 0:
            wnid = sc_list[0]
            sc_list.pop(0)
            tmp_synset = wn._synset_from_pos_and_offset('n', int(wnid[1:]))
            if len(tmp_synset.hypernyms()) > 0:
                superclass_list = tmp_synset.hypernyms()
                for sc in superclass_list:
                    offset = sc.offset()
                    sc_wnid = "n{:08d}".format(offset)
                    sc_list.append(sc_wnid)
                    if sc_wnid not in sc_membership.keys():
                        sc_membership[sc_wnid] = list()
                    if wnid in sc_membership.keys():
                        sc_membership = unfold_to_base_classes(wnid, sc_wnid, sc_membership)
                    else:
                        if wnid not in sc_membership[sc_wnid]:
                            sc_membership[sc_wnid].append(wnid)

    return sc_membership

def instance_class_membership(sc_membership, instance_class_ids):
    all_class_ids = list()

    for k in instance_class_ids:
        ic_membership[k] = list()
    
    for k in sc_membership.keys():
        all_class_ids.append(k)
        for member in sc_membership[k].keys():
            ic_membership[k].append(member)
            all_class_ids.append(member)

    all_class_ids.sort() 

    return ic_membership

def generate_hierarchy_labels(instance_membership, class_ids, ontology='wn'):
    vector_dict = dict()
    idx_dict = dict()
    key_list = sorted(instance_membership.keys())

    if ontology is 'wn':
        idx = 0
        for k in class_ids:
            idx_dict[k] = idx
            idx = idx + 1
        for k in instance_membership.keys():
            vector_dict[k] = [0]*len(class_ids)
            for member in instance_membership[k]:
                placeholder = 1
            
    return placeholder
     

def unfold_to_base_classes(wnid, sc_wnid, sc_membership):
    for member in sc_membership[wnid]:
        if member in sc_membership.keys():
            sc_membership = unfold_to_base_classes(member, sc_wnid, sc_membership)
        else:
            if member not in sc_membership[sc_wnid]:
                sc_membership[sc_wnid].append(member)

    return sc_membership

def get_synset(wnid):
    if isinstance(wnid, list):
        synset_list = list()
        for ID in wnid:
            synset_list.append(wn._synset_from_pos_and_offset(ID[0], int(ID[1:])))
        return synset_list
    else:
        return wn._synset_from_pos_and_offset(wnid[0], int(wnid[1:]))


# This function takes a dictionary, which may also contain dictionaries, and "unfolds" the
# dictionary to eliminate nesting, with new keys obeying the hdf5 format of hierarchical
# datasets, also prepending "key_substring" to the keys, and ensuring POSIX-style '/' delimiting
# target dict : dictionary to be unfolded 
# key_substring : string to be prepended to each key in dictionary
def unfold_dict(target_dict, key_substring=''):
    out_dict = dict()
    if not isinstance(key_substring, str) or not isinstance(target_dict, dict):
        raise TypeError('unexpected variable type')
    if len(key_substring) > 0:
        if not key_substring[0] is '/':
            key_substring = '/' + key_substring
    for k in target_dict:
        key_appended = key_substring + '/' + k
        if isinstance(target_dict[k], dict):
            new_target = target_dict[k]
            tmp_dict = unfold_dict(new_target, key_appended)
            for k in tmp_dict:
                out_dict[k] = tmp_dict[k]
        else:
            arr = target_dict[k]
            out_dict[key_appended] = target_dict[k]
    return out_dict


def write_to_hdf5(target_dict, out_path, file_permissions='w'):
        with h5py.File(out_path, file_permissions) as f:
            for k in target_dict:
                f.create_dataset(k, data=target_dict[k])

# used for determining if an entry is the last in an iterable object in order for treating this
# as a special case, which can be useful in some situations
def lookahead(iterable):
    it = iter(iterable)
    last = next(it)

    for val in it:
        yield last, True
        last = val
    yield last, False

# converts imagenet ids (string value from 0-999) to wnid (format $POS$offset)
def inid_to_wnid(idx_list, class_index_dict):
    wnid_list = list()
    for idx in idx_list:
        if not isinstance(idx, str):
            idx = str(idx)
        wnid_list.append(class_index_dict[idx][0])        

    return wnid_list

def wnid_to_inid(wnid_list, class_index_dict):
    inid_list = list()
    if not isinstance(wnid_list, list):
        wnid_list = [wnid_list]
    for wnid in wnid_list:
        for inid in class_index_dict.keys():
            if wnid == inid:
                inid_list.append(inid)
    return inid_list

def print_list_to_terminal(li, cols):
    name_count = 0    
    tmp_list = []
    for entry, has_more in lookahead(li):
        tmp_list.append(entry)
        name_count = name_count + 1    
        if name_count > (cols-1) or not has_more:
            for label, not_end in lookahead(tmp_list):
                if not_end:
                    print(label.ljust(15), end = '|')
                else:
                    print(label)
            tmp_list = []
            name_count = 0

def redirect_stdout(func, path, *params):
    original = sys.stdout
    sys.stdout = open(path, 'w')
    output = func(*params)
    sys.stdout = original

def normalize(x):
    return x / np.sqrt(np.sum(x**2, axis=1)[:, np.newaxis])

def get_default_class_dict(filepath='imagenet_class_index.json'):
    with open(filepath) as f:
        class_index_dict = json.load(f)

    return class_index_dict

def invert_dictionary(in_dict):
    out_dict = dict()
    val_list = list()
    for old_key in in_dict.keys():
        new_key = in_dict[old_key]
        new_val = old_key
        if new_val in val_list:
            raise ValueError('input dictionary must be one-to-one')
        out_dict[new_key] = new_val
        val_list.append(new_val)
    return out_dict

def create_filtered_subdirs(root_dir, subdir_list, keep_prob, output_dir='temp/'):
    all_subdirs = next(os.walk(root_dir))[1]
    binary_cntrl = np.random.choice([0,1], size=(len(subdir_list),), p=[keep_prob, 1-keep_prob])
    subdirs_to_remove = [subdir_list[i] for i, val in enumerate(binary_cntrl) if ~val]
    subdirs_to_keep = ( set(all_subdirs) ^ set(subdirs_to_remove) )

    for subdir in subdirs_to_keep:
        os.symlink(root_dir + '/' + subdir, output_dir)

    return subdirs_to_remove

# note - this function assumes the model contains an input layer, which will have as
#        the dimensions of the input as the layer.output_shape
# **** doesn't appear to be working properly ***
def calculate_memory_requirements(layers, batch_size=None ,datatype='float32'):
    bytes_per_mb = 1048576
    memory_by_type = {'float32':4, 'float64':8}
    num_weights = 0
    num_values = 0
    for layer in layers:
        if hasattr(layer, 'kernel'):
            tmp_prod = 1
            for dim in layer.kernel.shape:
                if dim is not None:
                    tmp_prod = tmp_prod * int(dim)
            num_weights = num_weights + tmp_prod
        if hasattr(layer, 'output_shape'):
            tmp_prod = 1
            for dim in layer.output_shape:
                if dim is not None:
                    tmp_prod = tmp_prod * dim
            if batch_size is not None:
                tmp_prod = tmp_prod * batch_size
            num_values = num_values + tmp_prod

    training_req = (3*num_weights + num_values) * memory_by_type[datatype] 
    training_req = training_req / bytes_per_mb
    testing_req = (num_weights + num_values) * memory_by_type[datatype]
    testing_req = testing_req / bytes_per_mb
    
    return training_req, testing_req, num_weights, num_values



