from tensorflow.keras.models import Model
from tensorflow.keras.models import Sequential
from tensorflow.keras.datasets import mnist
from tensorflow.keras import backend as K
from tensorflow.keras.layers import Flatten, Input

import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
import numpy as np
from numpy.core.umath_tests import inner1d

import argparse
import os
import pdb
import pickle

def create_exp_identifier(model_params):
    
    conv_depths = model_params['conv_depths']
    fc_depths = model_params['fc_depths']

    n_conv = len(conv_depths)
    n_fc = len(fc_depths)
    conv_nodes = 0
    fc_nodes = 0
    for node_count in conv_depths:
        conv_nodes += node_count
    for node_count in fc_depths:
        fc_nodes += node_count

    exp_identifier = 'conv_{:02d}_{:03d}_fc_{:02d}_{:04d}'.format(n_conv, conv_nodes,
                                                                  n_fc, fc_nodes)
    return exp_identifier


def init_ctd_entry(num_classes, layer_keys):

    ctd_dict = dict()
    for kl in layer_keys:
        ctd_dict[kl] = list()

    return ctd_dict

def extract_distance_vectors(dist_matrices, compiled_data=None, 
                                            normalize=False,
                                            layer_configs=None):
    dist_vectors = dict()

    for ke in sorted(dist_matrices.keys()):
        for kl in dist_matrices[ke].keys():
            tmp_mat = dist_matrices[ke][kl]
            for i in range(tmp_mat.shape[0]):
                kd = str(i)
                if kd not in dist_vectors.keys():
                    dist_vectors[kd] = dict()
                if kl not in dist_vectors[kd].keys():
                    dist_vectors[kd][kl] = list()
                if not normalize: 
                    dist_vectors[kd][kl].append(tmp_mat[i])
                else:
                    dist_vectors[kd][kl].append(tmp_mat[i]/sum(tmp_mat[i]))
                if compiled_data is not None:
                    compiled_data[kd][ke][kl].append(tmp_mat[i])
    if compiled_data is None:
        return dist_vectors
    else:
        return dist_vectors, compiled_data



def print_confusion_info(c_matrix, dist_matrices, epoch_key):

    ke = epoch_key
    for i in range(c_matrix.shape[0]):
        print('******* digit = ' + str(i) + ' *******')
        confusion_counts = np.transpose(c_matrix[:,i])
        print([c for c in confusion_counts.tolist()])
        print(list(reversed(np.argsort(confusion_counts.tolist()))))
        for kl in dist_matrices[ke].keys():
            digit_dists = dist_matrices[ke][kl][i]
            print(["{0:.02f}".format(d) for d in digit_dists])
            print(list(np.argsort(digit_dists)))
        print(c_matrix)
        print()
    print('[INFO] calculating distances complete.')

def compile_analysis_data(model_history,
                              model_params,
                              hist_data,
                              dist_matrices):
    
    analysis_data = dict()
    analysis_data['history'] = model_history
    analysis_data['model_params'] = model_params
    analysis_data['hist_data'] = hist_data
    analysis_data['dists'] = dist_matrices
    analysis_data['meta'], stats = analyze_results(analysis_data['dists'], 
                                                         analysis_data['hist_data'],
                                                         analysis_data['history'])
    return analysis_data, stats

def analyze_results(dists, histogram_data, history):
    z = 1
    mns_list = []
    stds_list = []
    maxs_list = []
    mins_list = []
    dist_stats = dict()
    for ke in sorted(dists.keys()):
        dist_stats[ke] = dict()
        for key_str in ('mean', 'std', 'max', 'min'):
            dist_stats[ke][key_str] = []
        for kl in sorted(dists[ke].keys()):
            A = dists[ke][kl]
            dist_mat = A[~np.eye(A.shape[0],dtype=bool)].reshape(A.shape[0],-1)

            dist_stats[ke]['mean'].append(np.mean(dist_mat, axis=1))
            dist_stats[ke]['std'].append(np.std(dist_mat, axis=1))
            dist_stats[ke]['max'].append(np.max(dist_mat, axis=1))
            dist_stats[ke]['min'].append(np.min(dist_mat, axis=1))
        
        mns_list.append(dist_stats[ke]['mean'])
        stds_list.append(dist_stats[ke]['std'])
        maxs_list.append(dist_stats[ke]['max'])
        mins_list.append(dist_stats[ke]['min'])

    compiled_stats = (mns_list, stds_list, maxs_list, mins_list)

    return dist_stats, compiled_stats

def visualize_results(analysis_data, model_history, outpath='metaplots/'):

    meta_data = analysis_data['meta']
    parsed_data = dict() 
    for ke in sorted(meta_data.keys()):
        epoch_data =  meta_data[ke]
        mean_data = epoch_data['mean']
        std_data = epoch_data['std']
        max_data = epoch_data['max']
        min_data = epoch_data['min']

        for digit_idx in range(len(mean_data[0])): 
            kd = str(digit_idx)
            if kd not in parsed_data.keys():
                parsed_data[kd] = dict()
                parsed_data[kd]['mean'] = [[] for _ in range(len(mean_data))]
                parsed_data[kd]['std'] = [[] for _ in range(len(mean_data))]
                parsed_data[kd]['max'] = [[] for _ in range(len(mean_data))]
                parsed_data[kd]['min'] = [[] for _ in range(len(mean_data))]
            for layer_idx in range(len(mean_data)):
                parsed_data[kd]['mean'][layer_idx].append(mean_data[layer_idx][digit_idx])
                parsed_data[kd]['std'][layer_idx].append(std_data[layer_idx][digit_idx])
                parsed_data[kd]['max'][layer_idx].append(max_data[layer_idx][digit_idx])
                parsed_data[kd]['min'][layer_idx].append(min_data[layer_idx][digit_idx])

    all_digits_data = dict()

    for kd in sorted(parsed_data.keys()):
        for ks in parsed_data[kd].keys():
            if ks not in all_digits_data.keys():
                all_digits_data[ks] = dict()
#               all_digits_data[ks] = [[] for _ in range(len(parsed_data[kd][ks]))]
            for kl in [str(x) for x in range(len(parsed_data[kd][ks]))]:
                if kl not in all_digits_data[ks].keys():
                    all_digits_data[ks][kl] = []
                all_digits_data[ks][kl].append(parsed_data[kd][ks][int(kl)])
    
    for ks in all_digits_data.keys():
        for kl in all_digits_data[ks].keys():
            Y = all_digits_data[ks][kl]
            labels = dict()
            labels['title'] = ks + ' across all digits for layer ' + kl
            labels['xlabel'] = 'epoch'
            labels['ylabel'] = ks
            labels['data'] = [str(i) for i in range(len(Y))]
            markr_specs = []
            X = [int(ke) for ke in sorted(meta_data.keys())]
            fn_out = outpath + "{}_all_digits_layer_{}.eps".format(ks,kl)
            if ks == 'std':
                history_subset = dict()
                history_subset['val_loss'] = model_history['val_loss']
                history_subset['loss'] = model_history['loss']
                save_plot(Y, X=X, labels=labels, outpath=fn_out,
                           x_lim=[0,np.max(X)+int(np.max(X)/10)],
                           y_lim=[0,np.max(Y)+0.1],
                           model_history=history_subset,
                           is_heatmap=False)
            else:
                save_plot(Y, X=X, labels=labels, outpath=fn_out,
                           x_lim=[0,np.max(X)+int(np.max(X)/10)],
                           y_lim=[0,1.1],
                           model_history=model_history,
                           is_heatmap=False)

    for kd in parsed_data.keys():
        for ks in parsed_data[kd].keys():
            Y = parsed_data[kd][ks]
            labels = dict()
            labels['title'] = ks + ' of distances for digit ' + kd + ' by layer'
            labels['xlabel'] = 'epoch'
            labels['ylabel'] = ks
            labels['data'] = [str(i) for i in range(len(Y))]
            markr_specs = []
            X = [int(ke) for ke in sorted(meta_data.keys())]
            fn_out = outpath + "{}_digit_{}.eps".format(ks,kd)
            save_plot(Y, X=X, labels=labels, outpath=fn_out,
                       x_lim=[0,np.max(X)],
                       y_lim=[0,1.1])

def write_param_files(params=None, 
                       base_fn_name='model_params',
                       outdir='param_files/'):
    if params is None:
        params = load_test_params()
    # filter ranges should be a 2-d tuple with first entry representing minimum
    # filter size and second entry representing max filter number
    cnn_chan_range = params['nfilter'][0]
    fc_chan_range = params['nfilter'][1]
    cnn_max = cnn_chan_range[1]
    fc_max = fc_chan_range[1]

    model_params = dict()
    model_params['conv_depths'] = [cnn_chan_range[0]] 
    model_params['fc_depths'] = [fc_chan_range[0]] 
    model_params['conv_pool'] = [True]

    file_count = 0
    for i in range(params['layers'][0]):
        for j in range(params['layers'][1]):
            if np.mod(j,2) == 0:
                continue
            print('j = ' + str(j))
            model_params['conv_depths'] = [cnn_chan_range[0]]*(i+1)
            model_params['conv_depths'] = [min(x, cnn_max) for x in model_params['conv_depths']]

            if i < 3:
                model_params['conv_pool'] = [True] + [False]*(len(model_params['conv_depths'])-1)
            else:
                model_params['conv_pool'] = [True, False, True, False] + \
                                                [False]*(len(model_params['conv_depths'])-4)

            model_params['fc_depths'] = [fc_chan_range[0]]*(j+1)
            model_params['fc_depths'] = [min(x, fc_max) for x in model_params['fc_depths']]

            out_path = outdir + base_fn_name + "_{:04d}".format(file_count) + '.pkl.'
#           with open(out_path, 'wb') as f:
#               pickle.dump(model_params, f)
            while True:
                out_path = outdir + base_fn_name + "_{:04d}".format(file_count) + '.pkl'
                with open(out_path, 'wb') as f:
                    pickle.dump(model_params, f)
                    file_count += 1
                print('file count = ' + str(file_count))
                print('conv_depths : ' + str(model_params['conv_depths']))
                print('conv_pool : ' + str(model_params['conv_pool']))
                print('fc_depths : ' + str(model_params['fc_depths']))
                print()
                if (np.min(model_params['fc_depths']) == fc_chan_range[1] and
                        np.min(model_params['conv_depths']) == cnn_chan_range[1]):
                    break
                                                                                      
                model_params['conv_depths'] = [min(x*2, cnn_max) for x in model_params['conv_depths']]
                model_params['fc_depths'] = [min(x*2, fc_max) for x in model_params['fc_depths']]
                out_path = outdir + base_fn_name + "_{:04d}".format(file_count) + '.pkl.'

def load_test_params():
    params = dict()
    params['nfilter'] = ((4,16), (8,64))
    params['layers'] = (4,8)

    return params

def write_digit_plots(dist_vectors, out_dir, data_labels, model_params, line_labels=None):
    labels = dict()
    labels['data'] = data_labels
    labels['xlabel'] = 'digit'
    labels['ylabel'] = 'distance'
    marker_specs = [u'']*len(labels['data'])

    for ki in dist_vectors.keys():
        for kl in dist_vectors[ki].keys():
            Y = dist_vectors[ki][kl] 
            if line_labels is None:
                labels['title'] = ("Digit {} in final fc layer".format(ki) + ':' +
                    str(model_params['conv_depths']) + ' | ' +
                    str(model_params['fc_depths']) )
            else:
                labels['title'] = ("Digit {} in final fc layer".format(ki) + ':' +
                    str(model_params['conv_depths']) + ' | ' +
                    str(model_params['fc_depths']) )

            plot_path = out_dir + "plot_digit_{}_layer_{}.eps".format(ki, kl)
            save_plot(Y, 
                      labels=labels, 
                      outpath=plot_path,
                      marker_specs=marker_specs)

def write_training_plots(model_history, 
                         hist_data, 
                         out_dir, 
                         n_epochs):
    fig = plt.figure()
    plt.plot(np.arange(0, n_epochs), model_history["loss"], label="train_loss")
    plt.plot(np.arange(0, n_epochs), model_history["val_loss"], label="val_loss")
    plt.plot(np.arange(0, n_epochs), model_history["accuracy"], label="train_acc")
    plt.plot(np.arange(0, n_epochs), model_history["val_accuracy"], label="val_acc")
    plt.title("Training Loss and Accuracy")
    plt.xlabel("Epoch #")
    plt.ylim([0,1])
    plt.ylabel("Loss/Accuracy")
    plt.legend()

    plt.savefig(out_dir + 'training_history.eps')
    plt.close(fig)
    print('...complete') 

def write_crosstraining_plots(cross_trial_data, trial_outdir):
    labels = dict()
    labels['data'] = list(range(10))
    labels['xlabel'] = 'digit'
    print("[INFO] writing cross training analysis plots...")
    for kd in cross_trial_data.keys():
        for ke in cross_trial_data[kd].keys():
            for kl in cross_trial_data[kd][ke].keys():
                tmp_data = cross_trial_data[kd][ke][kl]
                data_normalized = [d/np.sum(d) for d in tmp_data]
                labels['title'] = \
                    'Epoch {:03d} Digit {:01d} Layer {:01d}'.format(int(ke),int(kd),int(kl))
                labels['ylabel'] = 'distance'
                out_path = trial_outdir + \
                    'Epoch_{:03d}_Digit_{:01d}_Layer_{:01d}.eps'.format(int(ke),int(kd),int(kl))
                save_plot(tmp_data, outpath=out_path, labels=labels, is_heatmap=False)
    
                labels['title'] = \
                    'Epoch {:03d} Digit {:01d} Layer {:01d}'.format(int(ke),int(kd),int(kl))
                labels['ylabel'] = 'normalized distance'
                out_path = trial_outdir + \
                    'Epoch_{:03d}_Digit_{:01d}_Layer_{:01d}_normalized.eps'.format(int(ke),int(kd),int(kl))
                save_plot(data_normalized, outpath=out_path, 
                                                 labels=labels,
                                                 y_lim = [0, np.max(data_normalized)+.05],
                                                 is_heatmap=False)

def save_plot(Y, X=np.linspace(0,9,num=10), labels=None, 
              outpath='plot.eps',
              plot_type='line',
              marker_specs=None,
              reverse_cmap=True,
              y_lim=[0,1.2],
              x_lim=[-1,12],
              cmap='viridis',
              model_history=None,
              split_cmap=False,
              draw_legend=True,
              cmap_values=None,
              is_heatmap=True):
    if marker_specs is None:
        marker_specs = [u'']*len(labels['data'])
    else:
        marker_specs = [u'o']*len(labels['data'])
    plt.style.use("ggplot")
    fig = plt.figure()
    NCURVES = len(Y)
    color_vals = get_color_values(Y, values=cmap_values, c_map=cmap)
    
    if model_history is not None:
        for k in model_history.keys():
            train_stat = model_history[k]
            epochs = range(len(model_history[k]))
            plt.plot(epochs, train_stat)
    for (i,data_vec) in enumerate(Y):
        if plot_type == 'scatter':
            if marker_specs is None:
                plt.scatter(X, data_vec, label=labels['data'][i])
            else:
                plt.scatter(X, data_vec, 
                            marker = marker_specs['marker'][i],
                            c = marker_specs['c'][i],
                            label=labels['data'][i])
        else:
            if marker_specs is None:
                try:
                    plt.plot(X, data_vec, label=labels['data'][len(data_vec)-i])
                except:
                    plt.plot(X, data_vec)
            else:
                colorVal = color_vals[i]
                try:
                    plt.plot(X, data_vec, 
                                marker = marker_specs[i],
                                c = colorVal,
                                label=labels['data'][i])
                except:
                    plt.plot(X, data_vec, 
                             c = colorVal)
    if labels is not None:
        plt.title(labels['title'])
        plt.xlabel(labels['xlabel'])
        plt.ylabel(labels['ylabel'])
    if draw_legend:
        if is_heatmap:
            label_ints = [int(l) for l in labels['data']]
            norm = colors.Normalize(vmin=np.min(label_ints),
                                    vmax=np.max(label_ints))
            # create a ScalarMappable and initialize a data structure
            c_map = plt.get_cmap(cmap)
            c_map = colors.ListedColormap(c_map.colors[::-1])
            s_m = cmx.ScalarMappable(cmap=c_map, norm=norm)
#           s_m.set_array([])
            plt.colorbar(s_m)
        else:
            plt.legend(loc="lower right")
    axes = plt.gca()
    try:
        axes.set_ylim(y_lim)
        axes.set_xlim(x_lim)
    except:
        print('[INFO] could not set axes limit for {}, allowing default'.format(outpath))
    if outpath[-3:] == 'eps':
        plt.savefig(outpath, format='eps')
    else:
        plt.savefig(outpath)
    plt.close(fig)

def write_histograms(data_dict, weights, layer_keys, 
                     outdir='',
                     epoch=999):
    plt.style.use("ggplot")
    hist_data = dict()

    color_vals = get_color_values(layer_keys[1:])
    color_vals = list(reversed(color_vals))
    color_vals = [color_vals[0]] + color_vals

    probs = dict()
    bins = dict()
#   layer_keys = list(reversed(layer_keys))
    for k in sorted(data_dict.keys()):
        dens_bools = [False] + [True]*(len(data_dict[k])-1)
        num_bins = [100]*1 + [55]*3 + [180]*(len(data_dict[k])-4)
#       data_dict[k] = list(reversed(data_dict[k]))
        fig = plt.figure()
        probs[k] = []
        bins[k] = []
        for (i, data) in enumerate(data_dict[k]):
            if i == 0:
                alpha = 0.9
            else:
               #alpha = 0.45 + .005*i
                alpha = 1.0
            if data.ndim > 1:
                data = data.flatten()
            if i == len(data_dict[k])-1:
                y, bin_edges, _ = plot_histogram(data, bins=num_bins[i],
                                             alpha=alpha,
                                             density=dens_bools[i])
            else:
                y, bin_edges, _ = plot_histogram(data, bins=num_bins[i],
                                             alpha=alpha,
                                             density=dens_bools[i],
                                             color=color_vals[i],
                                             label=layer_keys[i-1])
            bins[k].append(bin_edges)
            probs[k].append(y)
           #if i == len(data_dict[k])-1:
            if i == 0:
                plt.title('Distribution of activations for digit ' + str(k) + ' final cnn layer')
                axes = plt.gca()
                axes.set_xlim([0,0.5])
                
                path_out = (outdir +
                    "digit_{:02d}_epoch_{:03d}_cnnlayer_activations.eps".format(int(k),int(epoch)))
                plt.savefig(path_out)
                axes.clear()
                plt.close(fig)
                fig = plt.figure()
            elif i == len(data_dict[k])-2:
                plt.title('Distributions of activations for digit ' + str(k) + ' fc layers')
                axes = plt.gca()
                axes.set_ylim([0,np.max([max(a) for a in probs[k][1:]])+0.01])

                targ_lim = np.max([max(a) for a in probs[k][1:]]) * 0.005
                x_lim = np.max( np.where( probs[k][-1] > targ_lim ) )
                x_lim = bins[k][-1][x_lim]

                axes.set_xlim([-x_lim, x_lim]) 
                path_out = (outdir +
                    "digit_{:02d}_epoch_{:03d}_fclayers_activations.eps".format(int(k),int(epoch)))
                plt.legend(loc="upper right")
                plt.savefig(path_out)
                plt.close(fig)
        plt.close(fig)
        probs[k] = list(reversed(probs[k]))
        bins[k] = list(reversed(bins[k]))
    plt.close(fig)
        
    hist_data['bins'] = bins
    hist_data['probs'] = probs

    return hist_data

def plot_histogram(data, **kwargs):
    (hist_vals, bin_edges, patches) =  plt.hist(data, **kwargs)

    return hist_vals, bin_edges, patches


def get_color_values(data, 
                     reverse_map=True, 
                     c_map='viridis',
                     split_cmap=False,
                     values=None,
                     cut_val=0.0):
        
    if values is None:
        values = np.linspace(cut_val, 1.0, num=len(data))
    else:
        cmin = values[0] - cut_val*values[0]
#   else:
#       values = values/np.max(values)
#       values = values - np.min(values)
        
    c_map = plt.get_cmap(c_map) 
#   c_norm  = colors.Normalize(vmin=0, vmax=values[-1])
    c_norm  = colors.Normalize(vmin=values[0], vmax=values[-1])
    if reverse_map:
        values = list(reversed(values))
    scalar_map = cmx.ScalarMappable(norm=c_norm, cmap=c_map)
    color_vals = []
    for v in values:
        color_vals.append(scalar_map.to_rgba(v))

    if split_cmap:
        color_vals = color_vals[len(color_vals)/2,:]

    return color_vals

def search(values, target):
    for k in values:
        for v in values[k]:
            if target in v:
                return k
    return None

def save_obj(obj, name):
    with open(name, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    with open( name, 'rb') as f:
        return pickle.load(f)

def backup_dictfile(name):
    src = 'obj/' + name + '.pkl'
    dst = 'obj/tmp_check/' + name + '.pkl'
    copyfile(src, dst)

def get_validation_data(dataset, normalize=True):
    if dataset == 'mnist':
        ((_, _), (X, Y)) = mnist.load_data()
        if K.image_data_format() == "channels_first":
            X = X.reshape((X.shape[0], 1, 28, 28))
        else:
            X = X.reshape((X.shape[0], 28, 28, 1))
        if normalize:
            X = X.astype("float") / 255.0 
            X = X - np.mean(X, axis=0)
            
    return X, Y

def calculate_distance_matrix(R, layer_keys, num_classes):
    dist_matrix = dict()
    for (idx, kl) in enumerate(layer_keys):
        dist_matrix[kl] = np.zeros((num_classes, num_classes))
        for kr1 in sorted(R.keys()):
            i = int(kr1)
            for kr2 in sorted(R.keys()):
                j = int(kr2)
                if kr1 is kr2:
                    continue
                else:
                    try:
                        class_sim = generalized_cosine(R[kr1][idx], R[kr2][idx])
                    except:
                        pdb.set_trace()
                    dist_matrix[kl][i][j] = 1 - class_sim
                    dist_matrix[kl][j][i] = dist_matrix[kl][i][j]

    return dist_matrix

def generalized_cosine(A,B,speedup=True):
    cos_AB = np.sum(inner1d(A, B)) / \
        np.sqrt( np.sum(inner1d(A, A)) * np.sum(inner1d(B, B)) )
    return cos_AB

def calc_R(x,normalize=False,white_noise=0):
    if normalize == True:
        x = x / np.sqrt(np.sum(x**2,axis=1)[:,np.newaxis])
    result = np.tensordot(x,x,axes=[0,0]) / np.shape(x)[0]
    if white_noise > 0:
        unit = np.diag(np.ones(np.shape(x)[1]))
        result = (1-white_noise)*result + white_noise/np.shape(x)[1]*unit
    return result

def sort_data(data, labels):
    data_sorted = dict()
    for i in range(0,10):
        data_sorted[str(i)] = []
    for (idx,entry) in enumerate(data):
        data_sorted[str(labels[idx])].append(list(entry))
    
    for k in data_sorted.keys():
        data_sorted[k] = np.array(data_sorted[k])
    
    return data_sorted

def gather_activations(base_model, data_in,
                       write_hists=False,
                       outdir='',
                       epoch=None,
                       layer_keys=None,
                       include_input=False):
    activations = dict()
    weights = dict()
    layer_info = []
    for (idx,k) in enumerate(data_in.keys()):
        activations[k] = []
        weights[k] = []
        if include_input:
            out = data_in[k].reshape(data_in[k].shape[0], -1)
            activations[k].append(out)
            weights[k].append([])
            
        for layer in base_model.layers:
            if layer.name.find('flatten') >= 0:
                continue
            if idx == 0:
                layer_info.append(layer.get_config())

            model = Model(base_model.input, outputs=layer.output)
            if layer.name.find('conv') >= 0 or \
                    layer.name.find('pool') >= 0:
                flatten = Flatten()(layer.output)
                model = Model(inputs=base_model.input, outputs=flatten)

            out = model.predict(data_in[k])
            activations[k].append(out)
            try:
                weights[k].append(layer.weights)
            except:
                weights[k].append([])
        if layer_keys is None:
            layer_keys = [str(x) for x in range(len(activations[k]))]
    test = activations['0']

    if write_hists:
        hist_data = write_histograms(activations, weights, layer_keys,
                                     epoch=epoch,
                                     outdir=outdir)
        return activations, layer_keys, layer_info, hist_data
    else:
        return activations, layer_keys, layer_info

def get_layer_labels(layer_configs):
    return None
