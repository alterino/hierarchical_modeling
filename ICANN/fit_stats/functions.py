# code based on
# https://pythonhealthcare.org/2018/05/03/81-distribution-fitting-to-data/

import pandas as pd
import numpy as np
import scipy
from sklearn.preprocessing import StandardScaler
import scipy.stats
import matplotlib.pyplot as plt

import pickle
import pdb



def get_p_value(data, distribution, params):
    
    sc=StandardScaler() 
    yy = data.reshape(-1,1)
    sc.fit(yy)
    y_norm = sc.transform(yy)
    y_norm = y_norm.flatten()
    p = scipy.stats.kstest(y_norm, 
                          distribution, 
                          args=params)[1]
    return p

def fit_distribution(data, 
                     random=False, 
                     nbins=12):
#   x = np.arange(len(data))
    x = np.linspace(0,1,nbins)
    size = len(data)

    sc=StandardScaler() 
    yy = data.reshape(-1,1)
    sc.fit(yy)
    y_norm = sc.transform(yy)
    y_norm = y_norm.flatten()

    dist_names = ['beta', 'expon', 'gamma', 
                     'lognorm', 'pearson3', 'triang',
                     'uniform', 'weibull_min', 'weibull_max']

    # Set up empty lists to stroe results
    chi_square = []
    p_values = []
    
    # Set up 50 bins for chi-square test
    # Observed data will be approximately evenly distrubuted aross all bins
    percentile_bins = np.linspace(0,100,nbins)
    percentile_cutoffs = np.percentile(y_norm, percentile_bins)
    observed_frequency, bins = (np.histogram(y_norm, bins=percentile_cutoffs))
    cum_observed_frequency = np.cumsum(observed_frequency)
    
    # Loop through candidate distributions
    for distribution in dist_names:
        # Set up distribution and get fitted distribution parameters
        dist = getattr(scipy.stats, distribution)
        param = dist.fit(y_norm)
         
        p = scipy.stats.kstest(y_norm, distribution, args=param)[1]
        p = np.around(p, 5)
        p_values.append(p) 
    
        cdf_fitted = dist.cdf(percentile_cutoffs, *param[:-2],
                              loc=param[-2], scale=param[-1])
    
    
        expected_frequency = []
        for bin in range(len(percentile_bins)-1):
            expected_cdf_area = cdf_fitted[bin+1] - cdf_fitted[bin]
            expected_frequency.append(expected_cdf_area)
    
        expected_frequency = np.array(expected_frequency) * size
        cum_expected_frequency = np.cumsum(expected_frequency)
        ss = sum (((cum_expected_frequency - cum_observed_frequency) ** 2) /
                    cum_observed_frequency)
        chi_square.append(ss)

         
    
    # Collate results and sort by goodness of fit (best at top)
    
    results = pd.DataFrame()
    results['Distribution'] = dist_names
    results['chi_square'] = chi_square
    results['p_value'] = p_values
    results.sort_values(['chi_square'], inplace=True)
        
    # Report results
    
    print ('\nDistributions sorted by goodness of fit:')
    print ('----------------------------------------')
    print (results)

    # Divide the observed data into 100 bins for plotting (this can be changed)
    number_of_bins = nbins
    bin_cutoffs = np.linspace(np.percentile(data,0),
            np.percentile(data,99),number_of_bins)
    
    h = plt.hist(data, bins = bin_cutoffs, color='0.75', density=True)

    number_distributions_to_plot = 1
    dist_names = results['Distribution'].iloc[0:number_distributions_to_plot]

    parameters = []

    for (idx, dist_name) in enumerate(dist_names):
        # Set up distribution and store distribution parameters
        dist = getattr(scipy.stats, dist_name)
        param = dist.fit(data)
        if idx == 0:
            params_normalized = dist.fit(y_norm)
            best_dist = dist_name
        parameters.append(param)

        # Get line for each distribution (and scale to match observed data)
        pdf_fitted = dist.pdf(bin_cutoffs, *param[:-2], loc=param[-2],
                              scale=param[-1])
        scale_pdf = np.trapz (h[0], h[1][:-1]) / np.trapz(pdf_fitted, bin_cutoffs)
        pdf_fitted *= scale_pdf


        # Add the line to the plot
        plt.plot(bin_cutoffs, pdf_fitted, label=dist_name)
            
        # Set the plot x axis to contain 99% of the data
        # This can be removed, but sometimes outlier data makes
        # the plot less clear
        plt.xlim(np.percentile(data,0),np.percentile(data,99))
        plt.xlabel("F1 score")

    # Store distribution paraemters in a dataframe (this could also be
    # saved)
    dist_parameters = pd.DataFrame()
    dist_parameters['Distribution'] = (
                    results['Distribution'].iloc[0:number_distributions_to_plot])
    try:
        dist_parameters['Distribution parameters'] = parameters
    except:
        pdb.set_trace()

    out_parameters = dict()
    out_parameters['Distribution'] = best_dist
    out_parameters['Distribution parameters'] = params_normalized

    out_parameters = (dist_parameters, out_parameters)
    # Print parameter results
    print ('\nDistribution parameters:')
    print ('------------------------')

    for index, row in dist_parameters.iterrows():
        print('\nDistribution:', row[0])
        print('Parameters:', row[1] )
    
    plt.legend()
    if random is False:
        plt.title('activation data (VGG16 fc layers), p = %.03f, mean = %.02f' % 
                    (results['p_value'].iloc(0)[0], sc.mean_))
        hist_fn = 'plots/imagenet/hist_with_curve_both_layers.eps'
        params_fn = 'empirical_params.pkl'
    else:
        plt.title('Permuted Data, p = %.03f, mean = %.02f' %
                    (results['p_value'].iloc(0)[0], sc.mean_))
        hist_fn = 'plots/imagenet/hist_with_curve_random.eps'
        params_fn = 'random_params.pkl'
    with open(params_fn, 'wb') as f:
        pickle.dump(out_parameters, f)
    plt.grid(zorder=0)
    plt.savefig(hist_fn)
