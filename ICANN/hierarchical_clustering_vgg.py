import os
import sys
import pdb
import time
import h5py
import utils
import json
import fit_stats
import numpy as np
import pickle

from nltk.corpus import wordnet as wn
from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.spatial.distance import squareform
from matplotlib import pyplot as plt

import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-p", "--params", type=str, default=None)
ap.add_argument("-g", "--gpu", type=int, default=1)
ap.add_argument("-a", "--architecture", type=str, default='vgg16')
args = vars(ap.parse_args())
#import sys
#sys.setrecursionlimit(1500)

plotit = False
write_plots=True
log_results=True
#num_layers = 49

#class_index_filepath = os.path.expanduser('~/.keras/models/imagenet_class_index.json')
class_index_filepath = 'imagenet_class_index.json'
with open(class_index_filepath) as f:
    class_index_dict = json.load(f)

data_dir = os.path.expanduser('~/data/conceptors/analysis/')
fn = args['architecture'] + '_imagenet.hdf5'
plot_dir = 'plots/imagenet/' + args['architecture'] + '/'
if not os.path.isdir(plot_dir):
    os.mkdir(plot_dir)
# data_collection_name = 'cosine_vals/vgg16_imagenet/'
sim_mats = dict()
dist_mats = dict()
dist_vec = dict()
clustering = dict()

labelList = range(1,1001)

with h5py.File(data_dir + fn, 'r') as dataset:
    data = dataset['cosine_vals/vgg16_imagenet']
    for k in data:
        sim_mats[k] = data[k][:]
        dist_mats[k] = 1 - sim_mats[k]
        dist_vec[k] = squareform(dist_mats[k])
        dist_vec[k] = np.random.permutation(dist_vec[k])
        clustering[k] = linkage(dist_vec[k],
                                method='average')
        if plotit == True:
            plt.figure(figsize=(10,7))
            dendrogram(clustering[k],
                       orientation='top',
                       labels=labelList,
                       distance_sort='descending',
                       show_leaf_counts=True)
            plt.title('agglomerative clustering for layer %s' % k)
            plt.show()
print('extracting base hierarchy... ', end='')
base_hierarchy = utils.extract_base_hierarchy(class_index_dict)
print('complete.')

nn_hierarchy = dict()
return_labels = True
sim_dict = dict()

f1_vec = list()
recall_vec = list()
precision_vec = list()

F1_scores = dict()
F1s_compiled = list()
mc_thresh = list(range(1,50))
chosen_thresh = 15

last_top_wnid = 0
last_bottom_wnid = 0

for k in sorted(clustering.keys()):
#   if int(k) < 40:
#       continue
    top_F1s = [0]*10
    top_groupdata = [dict() for _ in range(10)] 
    bottom_F1s = [2]*10
    bottom_groupdata = [dict() for _ in range(10)] 
    F1_scores[k] = dict()
    for i in mc_thresh:
        F1_scores[k][str(i)] = list()
#   if int(k) < 30:
#       continue
    print('layer ' + k)
    print('parsing linkage... ', end='')
    nn_hierarchy[k] = utils.parse_linkage(clustering[k], return_labels, class_index_dict)
    print('complete.')
    name_count = 0
    tmp_list = []
    sim_dict[k] = dict()
    for kb in sorted(base_hierarchy.keys()):
        group_B = base_hierarchy[kb]
        synset_B = wn._synset_from_pos_and_offset(kb[0], int(kb[1:]))
        depth_B = synset_B.max_depth()
        sim_dict[k][kb] = dict()
        max_F1 = 0
        for kn in reversed(sorted(nn_hierarchy[k].keys())):
            if int(kn) < 1000:
                continue
            group_A_inids = nn_hierarchy[k][kn]['leaves']
            group_A = utils.inid_to_wnid(group_A_inids, class_index_dict)
            depth_A = nn_hierarchy[k][kn]['node_depth']
            
            depths = (depth_A+1, depth_B+1)
            get_full_stats = True
            stats = utils.class_similarity(group_A, group_B, get_full_stats)
            F1 = stats[0]
            if F1 > max_F1:
                max_F1 = F1
                recall = stats[1]
                precision = stats[2]
                tp = set(group_A) & set(group_B)
                fp = set(group_A) - set(group_B)
                fn = set(group_B) - set(group_A)
                matching_group = group_A
                matching_group_inids = group_A_inids
                match_depth = depth_A
                member_count = len(group_B)
                sim_dict[k][kb]['max_similarity'] = max_F1
                sim_dict[k][kb]['closest_cluster'] = matching_group
                sim_dict[k][kb]['tp'] = tp
                sim_dict[k][kb]['fp'] = fp
                sim_dict[k][kb]['fn'] = fn
                sim_dict[k][kb]['recall'] = recall
                sim_dict[k][kb]['precision'] = precision
                sim_dict[k][kb]['member_count'] = member_count
            
        if len(matching_group) < 1000 and member_count > chosen_thresh:
            F1s_compiled.append(max_F1)
            if max_F1 > top_F1s[-1]:
                top_F1s[-1] = max_F1
                top_groupdata[-1]['class'] = synset_B.name()
                top_groupdata[-1]['wnid'] = kb
                top_groupdata[-1]['recall'] = recall
                top_groupdata[-1]['precision'] = precision
                top_groupdata[-1]['F1'] = max_F1
                top_groupdata[-1]['member_count'] = member_count
                new_idxs = list(reversed(np.argsort(top_F1s)))
                top_F1s = [top_F1s[idx] for idx in new_idxs]
                top_groupdata = [top_groupdata[idx] for idx in new_idxs]

            if max_F1 < bottom_F1s[-1]:
                bottom_F1s[-1] = max_F1
                bottom_groupdata[-1]['class'] = synset_B.name()
                bottom_groupdata[-1]['wnid'] = kb
                bottom_groupdata[-1]['recall'] = recall
                bottom_groupdata[-1]['precision'] = precision
                bottom_groupdata[-1]['F1'] = max_F1
                bottom_groupdata[-1]['member_count'] = member_count
                new_idxs = list(np.argsort(bottom_F1s))
                bottom_F1s = [bottom_F1s[idx] for idx in new_idxs]
                bottom_groupdata = [bottom_groupdata[idx] for idx in new_idxs]
        for kf1 in F1_scores[k].keys():
            if len(matching_group) < 1000 and member_count > int(kf1):
                F1_scores[k][kf1].append(max_F1)

        group_name = synset_B.name()
        matching_leaves = utils.inid_to_semantic_label(matching_group_inids, class_index_dict)

    if log_results:
        log_filename = os.path.join(plot_dir, 'results_log.txt')
        log_file = open(log_filename,'a')
        orig_stdout, sys.stdout = sys.stdout, log_file
    print()
    print('Top 10 F1 group data, layer %s' % k)
    for (idx, entry) in enumerate(top_groupdata):
        classname = entry['class']
        recall = entry['recall']
        precision = entry['precision']
        F1 = entry['F1']
        member_count = entry['member_count']
        wnid = entry['wnid']
        print('(%i) : %s : %s : recall=%.03f, precision=%.03f, F1=%.03f, member_count=%i' %
                (idx, wnid, classname, recall, precision, F1, member_count) )
    
    print()
    print('Bottom 10 F1 group data, layer %s' % k)
    for (idx, entry) in enumerate(bottom_groupdata):
        classname = entry['class']
        recall = entry['recall']
        precision = entry['precision']
        F1 = entry['F1']
        member_count = entry['member_count']
        wnid = entry['wnid']
        print('(%i) : %s : %s : recall=%.03f, precision=%.03f, F1=%.03f, member_count=%i' %
                (idx, wnid, classname, recall, precision, F1, member_count) )

    if log_results:
        sys.stdout = orig_stdout
        log_file.close()
data = np.asarray(F1s_compiled)

with open(os.path.join(plot_dir, 'random_data_backup.pkl'), 'wb') as f:
    save_data = (clustering, dist_mats, F1_scores, top_groupdata, bottom_groupdata)
    pickle.dump(save_data, f)
with open('random_params.pkl', 'rb') as f:
    parameters = pickle.load(f)

#distribution = dist_parameters['Distribution'].iloc(0)
#params = dist_parameters['Distribution parameters'].iloc(0)

dist_parameters = parameters[1]
distribution = dist_parameters['Distribution']
params = dist_parameters['Distribution parameters']

fit_stats.fit_distribution(data)
p_random = fit_stats.get_p_value(data, distribution, params)
z=1
ks_of_interest = ['fc1', 'fc2']
#ks_of_interest = [0,5,10,15,20,25,30,35,40,45,48]
#ks_of_interest = ['{:03d}'.format(k) for k in ks_of_interest]
kf1_pick = 15
kf1 = str(kf1_pick)
if write_plots:
    for k in ks_of_interest:
        if k not in F1_scores.keys():
            pdb.set_trace()
            continue
        fig = plt.figure()
        plt.grid(linestyle='--',zorder=0)
        datapoint_cnt = len(F1_scores[k][kf1])
        y, bin_edges, _ = utils.plot_histogram(F1_scores[k][kf1], 
                                               density=False,
                                               facecolor='green',
                                               alpha=0.8,
                                               bins=11,
                                               edgecolor='black',
                                               zorder=3)
    #                                          bins=min(max(int(datapoint_cnt/465)*30,10),15))
    #                                          bins=max(int(datapoint_cnt/7),5))
        axes = plt.gca()
        axes.set_ylim([0,1])
        axes.set_ylim([0,np.max(y)+0.01])
        axes.set_xlim([0,1]) 
        plt.title('layer %s' % k)
        plt.xlabel('F1 score')
        plt.ylabel('bin count')
        hist_fn = (plot_dir + 'randomized_hist_%s_mcthresh_%02d.eps' % (k, int(kf1)))
        plt.savefig(hist_fn)
        plt.close(fig)
        
        fig = plt.figure()
        plt.grid(linestyle='--',zorder=0)
        datapoint_cnt = len(F1_scores[k][kf1])
        y, bin_edges, _ = utils.plot_histogram(F1s_compiled, 
                                           density=False,
                                           facecolor='green',
                                           alpha=0.8,
                                           bins=11,
                                           edgecolor='black',
                                           zorder=3)
    #                                      bins=min(max(int(datapoint_cnt/465)*30,10),15))
    #                                      bins=max(int(datapoint_cnt/7),5))
        axes = plt.gca()
        axes.set_ylim([0,1])
        axes.set_ylim([0,np.max(y)+0.01])
        axes.set_xlim([0,1]) 
        plt.title('layer %s' % k)
        plt.xlabel('F1 score')
        plt.ylabel('bin count')
        hist_fn = (plot_dir + 'hist_both_mcthresh_%02d.eps' % (int(kf1)))
        plt.savefig(hist_fn)
        plt.close(fig)


#test_layer = 'fc2'
#test_F1s = sorted(F1_scores[test_layer][str(chosen_thresh)])






#data_filepath = os.path.expanduser('~/tmp/data/conceptors/analysis/presentation_data.hdf5')
#save_obj(sim_dict, data_filepath) 
            
#       depth = nn_hierarchy[k][k1]['node_depth']
#       member_count = nn_hierarchy[k][k1]['member_count']
#       print('cluster info for group %04i, depth = %i' % (int(k1), depth))
#       print('----------------------------------------------------\n')
#       for entry, has_more in utils.lookahead(nn_hierarchy[k][k1]['semantic_labels']):
#           tmp_list.append(entry)
#           name_count = name_count + 1    
#           if name_count > 2 or not has_more:
#               for label, not_end in utils.lookahead(tmp_list):
#                   label = label[1]
#                   if not_end:
#                       print(label.ljust(30), end = '')
#                   else:
#                       print(label)
#               tmp_list = []
#               name_count = 0
#       print('\n')
#       print('cluster info for group %04i, depth = %i, member_count = %i' 
#                                               % (int(k1), depth, member_count))
#       print('____________________________________________________\n')
#       print('\n')
#       pdb.set_trace()
