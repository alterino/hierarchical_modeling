import matplotlib
matplotlib.use("Agg")

from sklearn.metrics import classification_report, confusion_matrix
from sklearn.preprocessing import LabelBinarizer
from models import GenericNet
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.datasets import mnist
from tensorflow.keras import backend as K
from tensorflow import device 
from tensorflow.compat.v1.keras.backend import set_session
from tensorflow.compat.v1 import ConfigProto, Session
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
import numpy as np
from numpy.core.umath_tests import inner1d
import argparse
import os
import pdb
import glob

from pathlib import Path
import pickle
import shutil

import utils

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'

# to fix cudnn handle error
config = ConfigProto()
config.gpu_options.allow_growth = True
sess = Session(config=config)
set_session(sess)

ap = argparse.ArgumentParser()
ap.add_argument("-p", "--plot", type=str, default="plot.eps",
        help="path to output plot file")
ap.add_argument("-d", "--outdir", type=str, default='plots/')
ap.add_argument("-c", "--checkpoints", type=str, default="checkpoints/")
ap.add_argument("-e", "--exp", type=str, default='conv_02_008_fc_02_0016')
ap.add_argument("-i", "--dataset", type=str, default="mnist")
ap.add_argument("-g", "--gpu", type=int, default=1)
ap.add_argument("-w", "--write_plots", type=int, default=1)
args = vars(ap.parse_args())

if args['outdir'][-1] is not '/':
    args['outdir'] = args['outdir'] + '/'

exp_dir = 'checkpoints/' + args['dataset'] + '/fc_relu/' + args['exp'] + '/'
#exp_dir = args['checkpoints'] + args['exp'] + '/'
out_dir = args['outdir'] + 'finalized_' + args['dataset'] + '/' + args['exp'] + '/'

input_shape = (28, 28, 1)
input_dim = 784
num_classes = 10

if args['write_plots']:
    write_plots = True
else:
    write_plots = False

normalize_dists = False
print_confusion = True
compiled_analysis = True
write_overview = False

plot_type = 'line'
cross_trial_data = dict()
BASE_OUT_DIR = 'plots/' + args['dataset'] + '/' + args['exp'] + '/'
BASE_OUT_DIR = out_dir

for i in range(num_classes):
    cross_trial_data[str(i)] = dict()
for root, trial_dirs, files in os.walk(exp_dir): 
    for trial in reversed(trial_dirs): 
        skip_processing = False
        params_path = exp_dir + trial + '/' + 'model_params.pkl'
        try:
            with open(params_path, 'rb') as f:
                (model_params, model_history, time_str, c_matrix_orig) = pickle.load(f)
        except:
            print('[info] no params file, skipping ' + exp_dir)
            break

        checkpoint_dir = exp_dir + trial +'/'
        
        OUT_DIR = BASE_OUT_DIR + trial + '/'
        
        if os.path.isdir(OUT_DIR):
            try:
                with open(OUT_DIR + '/data_backup.pkl', 'rb') as f:
                    (dist_matrices, hist_data, _, layer_configs) = pickle.load(f)
                with open(BASE_OUT_DIR + '/cross_experiment_backup.pkl', 'rb') as f:
                    cross_trial_data = pickle.load(f)
                skip_processing = True
            except:
                try:
                    with open(OUT_DIR + '/data_backup.pkl', 'rb') as f:
                        (dist_matrices, hist_data, cross_trial_data, layer_configs) = pickle.load(f)
                    skip_processing = True
                except:
#                   pdb.set_trace()
                    shutil.rmtree(OUT_DIR)
                    skip_processing = False
                    write_overview = True
        else:
            write_overview = True
        if not skip_processing:
            Path(OUT_DIR).mkdir(parents=True)
            hists_dir = OUT_DIR + 'histograms/'
            if not os.path.isdir(hists_dir):
                Path(hists_dir).mkdir(parents=True)
            fn_pattern = 'weights-genericnet-*-*.hdf5'
            X_sorted = dict()
            
            X,Y = utils.get_validation_data('mnist', normalize=True) 
            
            X_sorted = utils.sort_data(X, Y)
                
            # convert the labels from integers and data to float
            lb = LabelBinarizer()
            Y = lb.fit_transform(Y)
            dist_matrices = dict()

#           files_of_interest = [1, 2, 4, 6, 8, 10, 15, 20, 25, 30, 40, 50, 60,
#                                75, 100]
            files_of_interest = [1, 2, 3, 4, 5, 7, 10, 15, 20, 25, 30, 35, 40, 50, \
                                    60, 70, 80, 90, 100]
            hist_data = dict()
            weights = dict()
            
            fn_list = glob.glob(checkpoint_dir + fn_pattern)
            
            with device('/gpu:' + str(args["gpu"])):
                for checkpoint_path in sorted(fn_list):
                    fn = checkpoint_path.split('/')[-1]
                    ke = fn[19:22]
                    if int(ke) not in files_of_interest:
                        continue
                    model = GenericNet.build(*input_shape, num_classes, **model_params)
                    try:
                        model.load_weights(checkpoint_path)
                    except:
                        pdb.set_trace()
                    print('[INFO] gathering activations for ' + checkpoint_path + '...') 
                    activations, layer_keys, layer_configs, hist_data[ke] = \
                                            utils.gather_activations(model, X_sorted,
                                                                     write_hists=True,
                                                                     outdir=hists_dir,
                                                                     epoch=ke,
                                                                     include_input=True)
                    print('[INFO] gathering activations complete.')
                    print('[INFO] generating confusion matrix')
                    predictions = model.predict(X, batch_size=128)
                    c_matrix = confusion_matrix(Y.argmax(axis=1), 
                                                predictions.argmax(axis=1))
                    for i in range(num_classes):
                        if ke not in cross_trial_data[str(i)].keys():
                            cross_trial_data[str(i)][ke] = utils.init_ctd_entry(num_classes,
                                                                                layer_keys)

                    print('[INFO] calculating correlation matrices for ' + checkpoint_path + '...') 
                    R = dict()
                    for k in activations.keys():
                        R[k] = []
                        for a in activations[k]:
                            R[k].append( utils.calc_R(a) )
                    print('[INFO] calculating correlation complete.')
                   
                    tmp_dict = dict()
                    print('[INFO] calculating distances for ' + checkpoint_path + '...') 
                    dist_matrices[ke] = utils.calculate_distance_matrix(R, layer_keys, num_classes)
                    if print_confusion:
                        utils.print_confusion_info(c_matrix, dist_matrices, ke)
        
            try:
                with open(OUT_DIR + 'data_backup.pkl', 'wb') as f:
                    pickle.dump((dist_matrices, hist_data, cross_trial_data, layer_configs), f)
            except:
                    pdb.set_trace()
        if write_plots:
            dist_vectors, cross_trial_data = \
                utils.extract_distance_vectors(dist_matrices, 
                                               compiled_data = cross_trial_data,
                                               normalize = normalize_dists)
#           layer_labels = utils.get_layer_labels(layer_configs)
            layer_labels = [info['name'] for info in layer_configs]
            if len(layer_labels) < len(dist_vectors['0'].keys()):
                layer_labels.insert(0, 'input')
            if not len(layer_labels) == len(dist_vectors['0'].keys()):
                pdb.set_trace()
            print('writing plots...')
            data_labels = sorted(dist_matrices.keys())
            NUM_EPOCHS = int(data_labels[-1])
            utils.write_digit_plots(dist_vectors, OUT_DIR, 
                                    sorted(dist_matrices.keys()), model_params)
            
            meta_dir = OUT_DIR + 'metaplots/'
            if not os.path.isdir(meta_dir):
                Path(meta_dir).mkdir(parents=True)
            utils.write_training_plots(model_history, 
                                       hist_data,
                                       meta_dir, 
                                       NUM_EPOCHS)

            print('[INFO] saving analysis data')
            analysis_data, stats = utils.compile_analysis_data(model_history,
                                                              model_params,
                                                              hist_data,
                                                              dist_matrices)
            utils.visualize_results(analysis_data, model_history, outpath=meta_dir)
            fileout_path = OUT_DIR + 'analysis_results.pkl'
            with open(fileout_path, 'wb') as f:
                pickle.dump(analysis_data, f)
            print('[INFO] output file: ' + fileout_path)
            print('[INFO] analysis complete.')
if write_overview:      
    with open(BASE_OUT_DIR + 'cross_experiment_backup.pkl', 'wb') as f:
        try:
            pickle.dump(cross_trial_data, f)
        except:
            pdb.set_trace()

if compiled_analysis:
    trial_outdir = BASE_OUT_DIR + 'cross_trial_analysis/'
    if not os.path.isdir(trial_outdir):
        Path(trial_outdir).mkdir(parents=True)
    labels = dict()
    labels['data'] = list(range(10))
    labels['xlabel'] = 'digit'
    print("[INFO] writing cross training analysis plots...")
    utils.write_crosstraining_plots(cross_trial_data, trial_outdir)
    print("[INFO] ...complete.")
z=1
            #files_of_interest = [2, 4, 6, 8, 9, 10, 12, 14, 16, 18, 20, 23, 25, 30, 35]
#           val_loss_min = np.min(model_history['val_loss'])
#           best_loss_epoch = np.where(model_history['val_loss'] == val_loss_min)
#           val_acc_max = np.min(model_history['val_loss'])
#           best_acc_epoch = np.where(model_history['val_accuracy'] == val_acc_max)
#           if best_loss_epoch[0] not in files_of_interest:
#               files_of_interest.append(best_loss_epoch[0])
#           if best_acc_epoch[0] not in files_of_interest:
#               files_of_interest.append(best_acc_epoch[0])
#           files_of_interest = sorted(files_of_interest)

#           analysis_data = dict()
#           analysis_data['history'] = model_history
#           analysis_data['model_params'] = model_params
#           analysis_data['hist_data'] = hist_data
#           analysis_data['dists'] = dist_matrices
#           print("trial = " + trial)
#           analysis_data['meta'], stats = utils.analyze_results(analysis_data['dists'], 
#                                                                 analysis_data['hist_data'],
#                                                                 analysis_data['history'])
