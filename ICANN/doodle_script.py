import matplotlib
matplotlib.use("Agg")

from sklearn.metrics import classification_report, confusion_matrix
from sklearn.preprocessing import LabelBinarizer
from models import GenericNet
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.datasets import mnist
from tensorflow.keras import backend as K
from tensorflow import device 
from tensorflow.compat.v1.keras.backend import set_session
from tensorflow.compat.v1 import ConfigProto, Session
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
import numpy as np
from numpy.core.umath_tests import inner1d
import argparse
import os
import pdb
import glob

from pathlib import Path
import pickle
import shutil
import time

import utils

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'

# to fix cudnn handle error
config = ConfigProto()
config.gpu_options.allow_growth = True
sess = Session(config=config)
set_session(sess)

ap = argparse.ArgumentParser()
ap.add_argument("-p", "--plot", type=str, default="plot.png",
        help="path to output plot file")
ap.add_argument("-d", "--outdir", type=str, default='plots/')
ap.add_argument("-c", "--checkpoints", type=str, default="checkpoints/")
ap.add_argument("-e", "--exp", type=str, default='conv_04_064_fc_08_0512')
ap.add_argument("-i", "--dataset", type=str, default="mnist")
ap.add_argument("-g", "--gpu", type=int, default=1)
ap.add_argument("-w", "--write_plots", type=int, default=1)
args = vars(ap.parse_args())

if args['outdir'][-1] is not '/':
    args['outdir'] = args['outdir'] + '/'

exp_dir = 'checkpoints/' + args['dataset'] + '/' + args['exp'] + '/'
out_dir = args['outdir'] + args['exp'] + '/'

input_shape = (28, 28, 1)
input_dim = 784
num_classes = 10

if args['write_plots']:
    write_plots = True
else:
    write_plots = False

normalize_dists = False
print_confusion = False
compiled_analysis = True
write_overview = False

plot_type = 'line'
cross_trial_data = dict()
BASE_OUT_DIR = 'plots/' + args['dataset'] + '/' + args['exp'] + '/'

for i in range(num_classes):
    cross_trial_data[str(i)] = dict()
for root, trial_dirs, files in os.walk(exp_dir): 
    for trial in trial_dirs: 
        skip_processing = False
        params_path = exp_dir + trial + '/' + 'model_params.pkl'
        try:
            with open(params_path, 'rb') as f:
                (model_params, model_history, time_str, c_matrix_orig) = pickle.load(f)
        except:
            print('[info] no params file, skipping ' + exp_dir)
            break

        checkpoint_dir = exp_dir + trial +'/'
        
        skip_processing = False
        if not skip_processing:
            fn_pattern = 'weights-genericnet-*-*.hdf5'
            X_sorted = dict()
            
            X,Y = utils.get_validation_data('mnist', normalize=True) 
            
            X_sorted = utils.sort_data(X, Y)
                
            # convert the labels from integers and data to float
            lb = LabelBinarizer()
            Y = lb.fit_transform(Y)
            dist_matrices = dict()

#           files_of_interest = [1, 2, 4, 6, 8, 10, 15, 20, 25, 30, 40, 50, 60,
#                                75, 100]
            files_of_interest = [1, 2, 3, 4, 5, 7, 10, 15, 20, 25, 30, 35, 40, 50, \
                                    60, 70, 80, 90, 100]
            hist_data = dict()
            weights = dict()
            
            fn_list = glob.glob(checkpoint_dir + fn_pattern)
            
            with device('/gpu:' + str(args["gpu"])):
                for checkpoint_path in sorted(fn_list):
                    fn = checkpoint_path.split('/')[-1]
                    ke = fn[19:22]
                    if int(ke) not in files_of_interest:
                        continue
                    model = GenericNet.build(*input_shape, num_classes, **model_params)
                    try:
                        model.load_weights(checkpoint_path)
                    except:
                        pdb.set_trace()
                    print('[INFO] gathering activations for ' + checkpoint_path + '...') 
                    activations, layer_keys = utils.gather_activations(model, X_sorted,
                                                                                    epoch=ke,
                                                                                    include_input=True)
                    print('[INFO] gathering activations complete.')
                    print('[INFO] generating confusion matrix')
                    predictions = model.predict(X, batch_size=128)
                    c_matrix = confusion_matrix(Y.argmax(axis=1), 
                                                predictions.argmax(axis=1))
                    for i in range(num_classes):
                        if ke not in cross_trial_data[str(i)].keys():
                            cross_trial_data[str(i)][ke] = utils.init_ctd_entry(num_classes,
                                                                                layer_keys)

                    print('[INFO] calculating correlation matrices for ' + checkpoint_path + '...') 
                    R = dict()
                    tmp = []
                    tot_time = 0
                    R1 = dict()
                    start_time = time.time()
#                   for k in activations.keys():
#                       R[k] = []
#                       for a in activations[k]:
#                           R[k].append( utils.calc_R(a) )
#                   print('t1 = %.4f' % time.time - start_time)
#                   print('[INFO] calculating correlation complete.')
#                   start_time = time.time()
#                   R2 = utils.tf.calc_R(activations)
#                   print('t2 = %.4f' % time.time - start_time)
                    for k in activations.keys():
                        R[k] = []
                        for (idx,a) in enumerate(activations[k]):
                            start_time = time.time()
                            R[k].append(utils.calc_R(a))
                            tot_time = tot_time + (time.time() - start_time)
                            tmp.append(a)
#                   print('t1 = %.6f seconds' % (tot_time))
#                   start_time = time.time()
#                   test = utils.tf.calc_R(tmp)
#                   print('t2 = %.6f seconds' % (time.time() - start_time))
#                           R[k].append( utils.calc_R(a) )
                    print('[INFO] calculating correlation complete.')
                    for k in activations.keys():
                        for r in R[k]:
                            start_time = time.time()
                            test1 = utils.generalized_cosine(r,r)
                            print('t1 = %.6f seconds, dim = %i' % (time.time() - start_time, r.shape[0]))
                            start_time = time.time()
                            test2 = utils.tf.generalized_cosine(r,r)
                            print('t2 = %.6f seconds, dim = %i' % (time.time() - start_time, r.shape[0]))
                    pdb.set_trace()
                    
                   
#                   tmp_dict = dict()
#                   print('[INFO] calculating distances for ' + checkpoint_path + '...') 
#                   dist_matrices[ke] = utils.calculate_distance_matrix(R, layer_keys, num_classes)
#                   if print_confusion:
#                       utils.print_confusion_info(c_matrix, dist_matrices, ke)
        
