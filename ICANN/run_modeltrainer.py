# USAGE
# python run_mnist_training.py

# import the necessary packages
from pyimagesearch.nn.conv import LeNet
from models import not_ShallowNet
from tensorflow.keras.callbacks import LearningRateScheduler, ModelCheckpoint
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.datasets import mnist
from tensorflow.compat.v1.keras.backend import set_session
from tensorflow.compat.v1 import ConfigProto, Session
from sklearn.preprocessing import LabelBinarizer
from sklearn.metrics import classification_report
from tensorflow.keras import backend as K
from tensorflow import device

import utils
import matplotlib.pyplot as plt
import numpy as np
import argparse
import os
import pdb

# to fix cudnn handle error
config = ConfigProto()
config.gpu_options.allow_growth = True
sess = Session(config=config)
set_session(sess)

# expects labels as integers
def relational_labels(labels, factor=0.1, verbose=False):
    orig_labels = labels
    label_dict = dict()
    for i in range(0,10):
        tmp_vec = np.zeros(10)
        for idx,val in enumerate(tmp_vec):
            if i == idx:
                tmp_vec[idx] = 0
            else:
                tmp_vec[idx] = 1/(abs(i - idx))
        tmp_vec = (tmp_vec / np.sum(tmp_vec))*factor
        tmp_vec[i] = (1-factor)
        label_dict[str(i)] = tmp_vec
    for idx,val in enumerate(labels):
        int_val = [i for i,x in enumerate(val) if x][0] 
        labels[idx] = label_dict[str(int_val)]

    if verbose is True:
        print("[INFO] smoothing amount: {}".format(factor))
        print("[INFO] before smoothing: {}".format(orig_labels[0]))
        print("[INFO] after smoothing: {}".format(labels[0]))
        print("[INFO] compiling model...")

    return labels

ap = argparse.ArgumentParser()
ap.add_argument("-s", "--smoothing", type=float, default=0.1,
        help="amount of label smoothing to be applied")
ap.add_argument("-p", "--plot", type=str, default="plot.png",
        help="path to output plot file")
ap.add_argument("-d", "--outdir", type=str, default="checkpoints")
ap.add_argument("-g", "--gpu", type=int, default=0)
args = vars(ap.parse_args())

epochs = 10
init_lr = 0.01
batch_size = 128
verbose = 1
FACTOR = args["smoothing"]

print("[INFO] accessing MNIST...")
((trainData, trainLabels), (testData, testLabels)) = mnist.load_data()

if K.image_data_format() == "channels_first":
    trainData = trainData.reshape((trainData.shape[0], 1, 28, 28))
    testData = testData.reshape((testData.shape[0], 1, 28, 28))

else:
    trainData = trainData.reshape((trainData.shape[0], 28, 28, 1))
    testData = testData.reshape((testData.shape[0], 28, 28, 1))

trainData = trainData.astype("float32") / 255.0
testData = testData.astype("float32") / 255.0

le = LabelBinarizer()
trainLabels = le.fit_transform(trainLabels)
trainLabels = trainLabels.astype("float")
# does not smooth test labels
testLabels = le.transform(testLabels)
validation_data = (testData, testLabels)
target_names = [str(x) for x in le.classes_] 

param_kws = ('validation_data', 'batch_size', 'epochs', 'callbacks', 'verbose')

with device('/gpu:' + str(args["gpu"])):
    opt = SGD(lr=init_lr)
    model = not_ShallowNet.build(width=28, height=28, depth=1, classes=10)
    model.compile(loss="categorical_crossentropy", optimizer=opt,
    	metrics=["accuracy"])
    
    #schedule = PolynomialDecay(maxEpochs=NUM_EPOCHS, initAlpha=INIT_LR,
    #        power=1.0)
    fname = os.path.sep.join([args["outdir"], 
        "weights-notShallow-" + "{:.2f}".format(FACTOR) + "-{epoch:03d}-{val_loss:.4f}.hdf5"])
    
    #callbacks = [LearningRateScheduler(schedule), ModelCheckpoint(fname)]
    callbacks = [ModelCheckpoint(fname,  monitor='val_loss', save_freq='epoch')]
    training_args = dict() 
    for kw in param_kws:
        training_args[kw] = locals()[kw]
    mt = utils.ModelTrainer(model, trainData, trainLabels,
                            train_args=training_args,
                            eval_args={'batch_size':128},
                            sfunc=relational_labels,
                            func_args={'factor' : FACTOR, 'verbose' : True},
                            classnames=target_names)
    pdb.set_trace()
    z=1
z = 1
    

# plot the training loss and accuracy
#plt.style.use("ggplot")
#plt.figure()
#plt.plot(np.arange(0, NUM_EPOCHS), H.history["loss"], label="train_loss")
#plt.plot(np.arange(0, NUM_EPOCHS), H.history["val_loss"], label="val_loss")
#plt.plot(np.arange(0, NUM_EPOCHS), H.history["accuracy"], label="train_acc")
#plt.plot(np.arange(0, NUM_EPOCHS), H.history["val_accuracy"], label="val_acc")
#plt.title("Training Loss and Accuracy")
#plt.xlabel("Epoch #")
#plt.ylabel("Loss/Accuracy")
#plt.legend()
#plt.show()
