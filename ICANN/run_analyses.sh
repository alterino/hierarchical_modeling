#!/bin/bash

for d1 in checkpoints/mnist/fc_relu/* ; do
    EXP_DIR=`basename "$d1"`
    PLOTS_DIR=plots/mnist/$EXP_DIR
#       echo "[BASH INFO] running analysis for exp. $EXP_DIR"
#       echo "[BASH INFO] running analysis for exp. $d1"
#       echo "[BASH INFO] running analysis for exp. $EXP_NUM"
#       echo "[BASH INFO] running analysis for exp. $d2"
    if  [ ! -d "$PLOTS_DIR" ]; then
        echo "[BASH INFO] running analysis for exp. $EXP_DIR"
        python run_analysis.py -e $EXP_DIR
    else
        echo "[BASH INFO] DETECTED DIRECTORY for experiment $EXP_DIR"
    fi
done
