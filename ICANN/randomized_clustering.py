import os
import pdb
import time
import h5py
import utils
import json

from nltk.corpus import wordnet as wn
from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.spatial.distance import squareform
from matplotlib import pyplot as plt
import numpy as np
import pickle

import fit_stats

plotit = False

#class_index_filepath = os.path.expanduser('~/.keras/models/imagenet_class_index.json')
class_index_filepath = 'imagenet_class_index.json'
with open(class_index_filepath) as f:
    class_index_dict = json.load(f)

data_dir = os.path.expanduser('~/data/conceptors/analysis/')
fn = 'vgg16_imagenet.hdf5'

# data_collection_name = 'cosine_vals/vgg16_imagenet/'
sim_mats = dict()
dist_mats = dict()
dist_vec = dict()
clustering = dict()

labels = list(range(1,1001))
dist_mats = list()
linkages = list()

num_trials = 5
num_classes = 1000

for _ in range(num_trials):
    dist_mats.append(utils.generate_random_distmat((num_classes, num_classes)))
    dist_vec = squareform(dist_mats[-1])
    linkages.append(linkage(dist_vec,
                            method='average'))
    if plotit == True:
        plt.figure(figsize=(10,7))
        dendrogram(linkages[-1],
                   orientation='top',
                   labels=labels,
                   distance_sort='descending',
                   show_leaf_counts=True)
        plt.title('agglomerative clustering for randomized distances')
        plt.show()

print('extracting base hierarchy... ', end='')
base_hierarchy = utils.extract_base_hierarchy(class_index_dict)
print('complete.')

nn_hierarchy = dict()
return_labels = True
sim_dict = dict()
f1_vec = list()
recall_vec = list()
precision_vec = list()
random_hierarchy = list()
sim_dict = list()
F1_scores = list()
F1s_compiled = list()
mc_threshold = 15
for i in range(len(linkages)):
    print('parsing linkage... ', end='')
    random_hierarchy.append(utils.parse_linkage(linkages[i], return_labels,
                            class_index_dict))
    print('complete.')
    name_count = 0
    tmp_list = []
    sim_dict.append(dict())
    F1_scores.append(list())
    for kb in sorted(base_hierarchy.keys()):
        group_B = base_hierarchy[kb]
        synset_B = wn._synset_from_pos_and_offset(kb[0], int(kb[1:]))
        depth_B = synset_B.max_depth()
        sim_dict[-1][kb] = dict()
        max_F1 = 0
        for kn in reversed(sorted(random_hierarchy[-1].keys())):
            if int(kn) < 1000:
                continue
            group_A_inids = random_hierarchy[-1][kn]['leaves']
            group_A = utils.inid_to_wnid(group_A_inids, class_index_dict)
            depth_A = random_hierarchy[-1][kn]['node_depth']
            
            depths = (depth_A+1, depth_B+1)
            get_full_stats = True
            stats = utils.class_similarity(group_A, group_B, get_full_stats)
            F1 = stats[0]
            if F1 > max_F1:
                max_F1 = F1
                recall = stats[1]
                precision = stats[2]
                tp = set(group_A) & set(group_B)
                fp = set(group_A) - set(group_B)
                fn = set(group_B) - set(group_A)
                matching_group = group_A
                matching_group_inids = group_A_inids
                match_depth = depth_A
                member_count = len(group_B)
                sim_dict[-1][kb]['max_similarity'] = max_F1
                sim_dict[-1][kb]['closest_cluster'] = matching_group
                sim_dict[-1][kb]['tp'] = tp
                sim_dict[-1][kb]['fp'] = fp
                sim_dict[-1][kb]['fn'] = fn
                sim_dict[-1][kb]['recall'] = recall
                sim_dict[-1][kb]['precision'] = precision
                sim_dict[-1][kb]['member_count'] = member_count
        if len(matching_group) < 1000 and member_count > mc_threshold:
            F1_scores[-1].append(max_F1)
            F1s_compiled.append(max_F1)

        group_name = synset_B.name()
        matching_leaves = utils.inid_to_semantic_label(matching_group_inids, class_index_dict)
        if  False:
#       if  len(matching_leaves) < 1000 and member_count > 10:
            print('-----------------------------------------------------------------')
            print('wordnet depth = %i, agglomerative depth = %i' % (depth_B, match_depth))
            print('Best match for cluster %s' % group_name)
            print('similarity = %.3f' % max_F1)
            utils.print_list_to_terminal( matching_leaves, 3)
            print('wordnet depth = %i, agglomerative depth = %i' % (depth_B, match_depth))
            print('Best match for cluster %s' % group_name)
            print('similarity = %.3f, results for random experiment, class length = %i' % 
                  (max_F1, member_count))
            print('recall = %.3f, precision = %.3f' % (recall, precision))
            print('-----------------------------------------------------------------')

#   for i in range(len(F1_scores)):
#       fig = plt.figure()
#       plt.grid(linestyle='--',zorder=0)
#       datapoint_cnt = len(F1_scores[i])
#       y, bin_edges, _ = utils.plot_histogram(F1_scores[i], 
#                                              density=False,
#                                              facecolor='blue',
#                                              alpha=0.9,
#                                              edgecolor='black',
#                                              bins=11,
#                                              zorder=3)
#                                              bins=min(max(int(datapoint_cnt/465)*30,10),15))
#                                              bins=max(int(datapoint_cnt/7),5))
#       axes = plt.gca()
#       axes.set_ylim([0,1])
#       axes.set_ylim([0,np.max(y)+0.01])
#       axes.set_xlim([0,1]) 
#       plt.title('Random-sampled Distance Matrix')
#       plt.xlabel('F1 score')
#       plt.ylabel('bin count')
#       hist_fn = ('plots/imagenet/hist_random_%02d.png' % (i))
#       plt.savefig(hist_fn)
#       plt.close(fig)

fig = plt.figure()
plt.grid(linestyle='--',zorder=0)
y, bin_edges, _ = utils.plot_histogram(F1s_compiled, 
                                       density=False,
                                       facecolor='blue',
                                       alpha=0.8,
                                       edgecolor='black',
                                       zorder=3)
#                                          bins=min(max(int(datapoint_cnt/465)*30,10),15))
#                                          bins=max(int(datapoint_cnt/7),5))
axes = plt.gca()
#   axes.set_ylim([0,1])
#   axes.set_ylim([0,np.max(y)+0.01])
axes.set_xlim([0,1]) 
plt.title('F1 Scores from %i Experiments' % num_trials)
plt.xlabel('F1 score')
plt.ylabel('bin count')
hist_fn = ('plots/imagenet/hist_random_all.png')
plt.savefig(hist_fn)
plt.close(fig)

data = np.asarray(F1s_compiled)

with open('empirical_params.pkl', 'rb') as f:
    parameters = pickle.load(f)

dist_parameters = parameters[1]
distribution = dist_parameters['Distribution']
params = dist_parameters['Distribution parameters']

fit_stats.fit_distribution(data, random=True)
p_empirical = fit_stats.get_p_value(data, distribution, params)
pdb.set_trace()
z = 0
#data_filepath = os.path.expanduser('~/tmp/data/conceptors/analysis/presentation_data.hdf5')
#save_obj(sim_dict, data_filepath) 
            
#       depth = random_hierarchy[-1][k1]['node_depth']
#       member_count = random_hierarchy[-1][k1]['member_count']
#       print('cluster info for group %04i, depth = %i' % (int(k1), depth))
#       print('----------------------------------------------------\n')
#       for entry, has_more in utils.lookahead(random_hierarchy[-1][k1]['semantic_labels']):
#           tmp_list.append(entry)
#           name_count = name_count + 1    
#           if name_count > 2 or not has_more:
#               for label, not_end in utils.lookahead(tmp_list):
#                   label = label[1]
#                   if not_end:
#                       print(label.ljust(30), end = '')
#                   else:
#                       print(label)
#               tmp_list = []
#               name_count = 0
#       print('\n')
#       print('cluster info for group %04i, depth = %i, member_count = %i' 
#                                               % (int(k1), depth, member_count))
#       print('____________________________________________________\n')
#       print('\n')
#       pdb.set_trace()
