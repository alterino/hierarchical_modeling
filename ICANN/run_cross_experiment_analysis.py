import matplotlib
matplotlib.use("Agg")

from sklearn.metrics import classification_report, confusion_matrix
from sklearn.preprocessing import LabelBinarizer
from models import GenericNet
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.datasets import mnist
from tensorflow.keras import backend as K
from tensorflow import device 
from tensorflow.compat.v1.keras.backend import set_session
from tensorflow.compat.v1 import ConfigProto, Session
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
import numpy as np
from numpy.core.umath_tests import inner1d
import argparse
import os
import pdb
import glob

from pathlib import Path
import pickle
import shutil

import utils

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'

# to fix cudnn handle error
config = ConfigProto()
config.gpu_options.allow_growth = True
sess = Session(config=config)
set_session(sess)

def get_similarity_stats(dists,
                         return_sims=False):
    sims = []

    for i in range(len(dists)):
        for j in range(i+1, len(dists)):
            sims.append(utils.generalized_cosine(dists[i],dists[j]))
    if return_sims:
        return np.mean(sims), np.std(sims)
    else:
        return np.mean(sims), np.std(sims), sims

def dict_to_plotable(data_dict, thresh=None):
    data_vectorized = []
    labels_vectorized = []

    sorted_keys = sorted([int(k) for k in data_dict.keys()])

    for key in sorted_keys:
#       if int(key) < 10:
#           pdb.set_trace()
        if thresh is not None:
            if int(key) < thresh:
                data_vectorized.append(data_dict[str(key)])
                labels_vectorized.append(key)
        else:
            data_vectorized.append(data_dict[str(key)])
            labels_vectorized.append(key)

    return data_vectorized, labels_vectorized

def write_cross_exp_plots(data_dict):
    z = 1

def sum_parameters(exp_info, conv_width, conv_input_dims):
    num_conv = int(exp_info[1])
    num_channels_per = int( int(exp_info[2]) / num_conv )
    num_fc = int(exp_info[4])
    num_nodes_per = int( int(exp_info[5]) / num_fc )
    param_sum = 0
    fc_input_dims = list()
    fc_input_dims.append(conv_input_dims)
    for _ in range(num_fc):
        fc_input_dims.append(num_nodes_per)

    for _ in range(num_conv):
        param_sum += conv_width**2 * num_channels_per
    for dim in fc_input_dims:
        param_sum += dim*num_nodes_per
    
    return param_sum

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--outdir", type=str, default='plots/')
ap.add_argument("-i", "--dataset", type=str, default='mnist')
ap.add_argument("-g", "--gpu", type=int, default=1)
args = vars(ap.parse_args())

if args['outdir'][-1] is not '/':
    args['outdir'] = args['outdir'] + '/'

finalized_analysis = True

if finalized_analysis:
    top_dir = args['outdir'] + 'finalized_' + args['dataset'] + '/'
else:
    top_dir = args['outdir'] + args['dataset'] + '/'
checkpoints_dir = 'checkpoints/' + args['dataset'] + '/fc_relu/'
out_dir = top_dir + 'cross_experiment_plots/'
if not os.path.isdir(out_dir):
    os.mkdir(out_dir)
#checkpoints_dir = 'checkpoints/' + args['dataset'] + '/'

input_shape = (28, 28, 1)
input_dim = 784
num_classes = 10

normalize_dists = False
print_confusion = True
compiled_analysis = False

dists_by_nparams = dict()
dists_by_nlayers = dict()
history_by_nparams = dict()
history_by_nlayers = dict()

loi_keys = dict()

layers_to_plot = [2, 2]
for root, experiments, files in os.walk(top_dir): 
    for exp_dir in experiments: 
        try:
            with open(top_dir + exp_dir + '/cross_experiment_backup.pkl', 'rb') as f:
                cross_trial_data = pickle.load(f)
        except:
            print('[info] skipping experiment directory: {}'.format(exp_dir))
            continue
        BASE_DIR = top_dir + exp_dir + '/'
        exp_info = exp_dir.split('_')
        num_params = int(exp_info[2]) + int(exp_info[5])
        if num_params == 6 or num_params == 8:
            continue
        knp = str(num_params)
        n_conv = int(exp_info[1])
        n_fc = int(exp_info[4])
#       if not (n_conv <= layers_to_plot[0] and n_fc <= layers_to_plot[1]) or \
#               num_params > 100:
#           continue
        num_layers = n_conv + n_fc
        knl = str(num_layers)
        if knp not in dists_by_nparams.keys():
            dists_by_nparams[knp] = dict()
            history_by_nparams[knp] = list()
        if knl not in dists_by_nlayers.keys():
            dists_by_nlayers[knl] = dict()
            history_by_nlayers[knl] = list()
        for _, trial_dirs, files in os.walk(BASE_DIR): 
            for trial in trial_dirs: 
                if trial == 'cross_trial_analysis':
                    continue

                cp_dir = checkpoints_dir + exp_dir + '/' + trial +'/'
                params_path = cp_dir + 'model_params.pkl'
                OUT_DIR = BASE_DIR + trial + '/'
                
#               with open(OUT_DIR + 'data_backup.pkl', 'rb') as f:
#                   (dist_matrices, hist_data, _) = pickle.load(f)

                fileout_path = OUT_DIR + 'analysis_results.pkl'
                try:
                    with open(params_path, 'rb') as f:
                        (model_params, model_history, time_str, c_matrix_orig) = pickle.load(f)
                    with open(fileout_path,'rb') as f:
                        analysis_data = pickle.load(f)
                except:
                    print('[info] skipping trial directory: {}/{}'.format(exp_dir, trial))
                    continue
                trial_outdir = BASE_DIR + 'cross_trial_analysis/'
                dist_matrices = analysis_data['dists']
                for ke in dist_matrices.keys():
                    if ke not in dists_by_nparams[knp].keys():
                        dists_by_nparams[knp][ke] = dict()
                    if ke not in dists_by_nlayers[knl].keys():
                        dists_by_nlayers[knl][ke] = dict()
                    for kl in dist_matrices[ke].keys():
                        if kl not in dists_by_nparams[knp][ke].keys():
                            dists_by_nparams[knp][ke][kl] = list()
                        if kl not in dists_by_nlayers[knl][ke].keys():
                            dists_by_nlayers[knl][ke][kl] = list()

                        dists_by_nparams[knp][ke][kl].append(dist_matrices[ke][kl])
                        dists_by_nlayers[knl][ke][kl].append(dist_matrices[ke][kl])
                history_by_nparams[knp].append(model_history['val_accuracy'])
                history_by_nlayers[knl].append(model_history['val_accuracy'])
#           test1 = dists_by_nparams[knp][ke]['1']
#           test2 = dists_by_nlayers[knl][ke]['1']
            break 
        loi_keys[knp] = sorted(list(dist_matrices[ke].keys())[-2])
    break


plot_data = dict()
plot_data['means'] = dict() 
plot_data['stds'] = dict()
epochs_idx = sorted([int(k) for k in dist_matrices.keys()])
epoch_keys = [str(idx).zfill(3) for idx in epochs_idx]
debug_sims = []
debug_sims_meta = []
for knp in dists_by_nparams.keys():
    kl = loi_keys[knp][0]
    fc_key = 'final_fc'
    pdb.set_trace()
    for ke in epoch_keys:
        if fc_key not in plot_data['means'].keys():
            plot_data['means'][fc_key] = dict()
            plot_data['stds'][fc_key] = dict()
        if knp not in plot_data['means'][fc_key].keys():
            plot_data['means'][fc_key][knp] = list()
            plot_data['stds'][fc_key][knp]= list()
        try:
            tmp_dists = dists_by_nparams[knp][ke][kl]
        except:
            pdb.set_trace()
        means_sim, stds_sim, sims = get_similarity_stats(tmp_dists)
        plot_data['means'][fc_key][knp].append(means_sim)
        plot_data['stds'][fc_key][knp].append(stds_sim)
        debug_sims.append(sims)
        debug_sims_meta.append((knp, kl))

test = str(dists_by_nparams.keys())
params = sorted([int(k) for k in dists_by_nparams.keys()])
param_keys = [str(k) for k in params]
lkeys_1 = sorted(dists_by_nparams[param_keys[0]][ke].keys())
lkeys_2 = sorted(dists_by_nparams[param_keys[-1]][ke].keys())

parsed_plot_data = dict()
parsed_plot_data['means'] = dict()
parsed_plot_data['stds'] = dict()
layer_keys = plot_data['means'].keys()

pdb.set_trac()

for kl in ['final_fc']:
    labels = dict()
    means_dict = plot_data['means'][kl]
    stds_dict = plot_data['stds'][kl]
    
    (y_m, labels_m) = dict_to_plotable(means_dict)
    (y_s, labels_s) = dict_to_plotable(stds_dict)
#   (y_m, labels_m) = dict_to_plotable(means_dict, thresh=50)
#   (y_s, labels_s) = dict_to_plotable(stds_dict, thresh=50)
    labels['xlabel'] = 'epoch'
    x_lim = [1, 100]
    mns_ylim = [0.0, 1.05]
    std_ylim = [0, np.min(np.max(y_s))+.05]
    mn_outpath = out_dir + 'mean_sim_vs_epoch_numparams_' + kl.zfill(2) + '.eps'
    stds_outpath = out_dir + 'std_sim_vs_epoch_numparams_' + kl.zfill(2) + '.eps'
    labels['ylabel'] = 'mean of similarity'
    labels['title'] = 'mean of similarities vs. epoch'
    labels['data'] = labels_m
    utils.save_plot(y_m, epochs_idx,
              x_lim=x_lim,
              y_lim = mns_ylim,
              labels=labels,
              outpath=mn_outpath,
              cmap_values=params,
              cmap='inferno',
              is_heatmap=False)
    labels['ylabel'] = 'std of similarity'
    labels['title'] = 'standard deviation of similarities vs. epoch, layer' + kl
    labels['data'] = labels_s
    utils.save_plot(y_s, epochs_idx,
              x_lim=x_lim,
              y_lim=std_ylim,
              labels=labels,
              outpath=stds_outpath,
              draw_legend=True,
              cmap_values=params,
              cmap='inferno',
              is_heatmap=False)
history_outpath = out_dir + 'model_history_by_numparams.eps'

(y_history, labels_history) = dict_to_plotable(history_by_nparams)
for (idx,hist) in enumerate(y_history):
    y_history[idx] = np.average(hist, axis=0)

x = range(1,len(y_history[0])+1)
x_lim = [1, 110]
history_ylim = [0.0, 1.05]

labels['ylabel'] = 'validation accuracy'
labels['title'] = 'validation accuracy across architectures'
labels['data'] = labels_history
utils.save_plot(y_history, x,
          x_lim=x_lim,
          y_lim=history_ylim,
          labels=labels,
          outpath=history_outpath,
          draw_legend=True,
          cmap_values=params,
          cmap='inferno',
          is_heatmap=False)
pdb.set_trace()

#if compiled_analysis:
#    trial_outdir = BASE_OUT_DIR + 'cross_trial_analysis/'
#    if not os.path.isdir(trial_outdir):
#        Path(trial_outdir).mkdir(parents=True)
#    labels = dict()
#    labels['data'] = list(range(10))
#    labels['xlabel'] = 'digit'
#    print("[INFO] writing cross training analysis plots...")
#    utils.write_crosstraining_plots(cross_trial_data, trial_outdir)
#    print("[INFO] ...complete.")
z=1


