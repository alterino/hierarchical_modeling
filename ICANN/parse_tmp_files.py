import os
import sys
import pdb
import re
import time
import json

import numpy as np
from utils import cluster_spectral 
import utils

class_index_filepath = os.path.expanduser('~/.keras/models/imagenet_class_index.json')

try:
    num_clusts = int(sys.argv[1])
except:
    num_clusts = 3

with open(class_index_filepath) as f:
    class_index_dict = json.load(f)

num_leaves = 1000
model_name = 'resnet50'

if model_name is 'resnet50':
    num_layers = 49
elif model_name is 'inceptionV3':
    num_layers = 95

data_dir = os.path.expanduser('~/data/conceptors/analysis/' + model_name + '/')
out_dir = os.path.expanduser('~/data/conceptors/analysis/')
out_fn = out_dir + model_name + '_imagenet.hdf5'

sim_mats = dict()
for i in range(0, num_layers):
    k = str(i).zfill(3)
    sim_mats[k] = np.zeros((num_leaves, num_leaves))
    for j in range(num_leaves):
        sim_mats[k][j][j] = 1

key_list_by_matrix_index = [''] * num_leaves

print('beginning parsing .txt files...')
for root, _, filenames in os.walk(data_dir):
    for fn in filenames:
        fn_parsed = re.split('[_.,]', fn)
        try:
            idx1 = int(fn_parsed[0])
            idx2 = int(fn_parsed[1])
        except:
            if fn == '.key_list.txt':
                with open(root + fn) as f:
                    i = 0
                    for line in f:
                        line = line.replace('\n', '')
                        key_list_by_matrix_index[i] = line
                        i=i+1
                continue
            else:
                print('error on file ' + fn)
                continue
        with open(root + fn) as f:
            for line in f:
                line_parsed = re.split(':', line)
                key = line_parsed[0]
                val = float(line_parsed[1].replace('\n', ''))
                try:
                    sim_mats[key][idx1][idx2] = val
                    sim_mats[key][idx2][idx1] = val
                except:
                    pdb.set_trace()
            if not int(key) == num_layers-1:
                pdb.set_trace()
                raise ValueError
        if idx2 == num_leaves - 1:
            print('completed files for row ' + str(idx1))
print('completed parsing .txt files\n')
utils.write_to_hdf5(sim_mats, out_fn)
pdb.set_trace()

#for i in range(num_leaves):
#    for j in range(num_leaves):
#        if sim_mats['fc1'][i][j]==0 or sim_mats['fc2'][i][j]==0:
#            print('\n********check values at index %i,%i*********\n' % (i, j))
        
            


#grp_fc1 = cluster_spectral(sim_mats['fc1'], num_clusts)
#grp_fc2 = cluster_spectral(sim_mats['fc2'], num_clusts)
#
#clusters = dict()
#clusters['fc1'] = list()
#clusters['fc2'] = list()
#
#for i in range(1,num_clusts+1):
#    clusters['fc1'].append(list())
#    clusters['fc2'].append(list())
#i = 0
#for nlist in clusters['fc1']:
#    i=i+1
#    for j in range(len(grp_fc1)):
#        if grp_fc1[j] == i:
#            nlist.append(class_index_dict[key_list_by_matrix_index[j]])
#
#i = 0
#for nlist in clusters['fc2']:
#    i=i+1
#    for j in range(len(grp_fc2)):
#        if grp_fc2[j] == i:
#            nlist.append(class_index_dict[key_list_by_matrix_index[j]])
#
#i = 0
##for nlist in clusters['fc1']:
##    i=i+1
##    for j in range(len(grp_fc1)):
##        if grp_fc1[j] == i:
##            nlist.append(class_index_dict[str(j)])
##
##i = 0
##for nlist in clusters['fc2']:
##    i=i+1
##    for j in range(len(grp_fc2)):
##        if grp_fc2[j] == i:
##            nlist.append(class_index_dict[str(j)])
##
#print('fc1 with clusters = %i\n' % num_clusts)
#for nlist in clusters['fc1']:
#    print nlist
#    print '-------------------------------------------------------------'
#print('\nfc2 with clusters = %i\n' % num_clusts)
#for nlist in clusters['fc2']:
#    print nlist 
#    print '-------------------------------------------------------------'
#print('\n')
