from models import GenericNet
#from models import not_ShallowNet
from tensorflow.keras.callbacks import LearningRateScheduler, ModelCheckpoint
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.datasets import cifar10
from tensorflow.compat.v1.keras.backend import set_session
from tensorflow.compat.v1 import ConfigProto, Session
from sklearn.preprocessing import LabelBinarizer
from sklearn.metrics import classification_report
from tensorflow.keras import backend as K
from tensorflow import device
import matplotlib.pyplot as plt
import numpy as np
import argparse
import os

import pdb
from pathlib import Path
from datetime import datetime
import time
import pickle

now = datetime.now()

# to fix cudnn handle error
config = ConfigProto()
config.gpu_options.allow_growth = True
sess = Session(config=config)
set_session(sess)

ap = argparse.ArgumentParser()
ap.add_argument("-p", "--plot", type=str, default="plot.png",
        help="path to output plot file")
ap.add_argument("-d", "--outdir", type=str, default="checkpoints/cifar10/")
ap.add_argument("-m", "--params", type=str, default=None)
ap.add_argument("-g", "--gpu", type=int, default=1)
args = vars(ap.parse_args())

if args['outdir'][-1] is not '/':
    args['outdir'] = args['outdir'] + '/'

# define the total number of epochs to train for initial learning
# rate, and batch size
NUM_EPOCHS = 100
INIT_LR = 5e-3
BATCH_SIZE = 128

input_shape = (32,32,3)
classes = 10 
if args['params'] is None:
    model_params = dict()
    model_params['conv_depths'] = [16, 16, 32, 32,64,64]
#   model_params['conv_depths'] = list(reversed(model_params['conv_depths']))
    model_params['fc_depths'] = [256,128]
#   model_params['fc_depths'] = list(reversed(model_params['fc_depths']))
    model_params['conv_pool'] = [False, False, False, False, False, True]
#   model_params['conv_pool'] = list(reversed(model_params['conv_pool']))
    model_params['conv_shape'] = (5,5)
else:
    with open(args['params'], 'rb') as f:
        model_params = pickle.load(f)
if 'conv_pool' not in model_params.keys():
    n_layers = (len(model_params['conv_depths']),
                                  len(model_params['fc_depths']))
    model_params['conv_pool'] = [False]*(n_layers[0]-1) + [True]

params_str = ('cv-{:02}'.format(len(model_params['conv_depths'])) +
                  '-{:03}'.format(model_params['conv_depths'][-1]) +
                  '-fc-{:02}'.format(len(model_params['fc_depths'])) + 
                  '-{:03}'.format(model_params['fc_depths'][-1]))
args['outdir'] = args['outdir'] + params_str + '/'
if not os.path.isdir(args['outdir']):
    Path(args['outdir']).mkdir(parents=False)

time_str = now.strftime("%y%m%d_%H%M")
tmp_outdir = (args['outdir'] + time_str + '/')
        
print("[INFO] accessing cifar10...")
((train_data, train_labels), (test_data, test_labels)) = cifar10.load_data()

if K.image_data_format() == "channels_first":
	train_data = train_data.reshape((train_data.shape[0], 
                                         input_shape[2], 
                                         input_shape[1], 
                                         input_shape[0]))
	test_data = test_data.reshape((test_data.shape[0], 
                                         input_shape[2], 
                                         input_shape[1], 
                                         input_shape[0]))


else:
	train_data = train_data.reshape((train_data.shape[0], 
                                         input_shape[0], 
                                         input_shape[1], 
                                         input_shape[2]))
	test_data = test_data.reshape((test_data.shape[0], 
                                         input_shape[0], 
                                         input_shape[1], 
                                         input_shape[2]))

train_data = train_data.astype("float32") / 255.0
test_data = test_data.astype("float32") / 255.0

le = LabelBinarizer()
train_labels = le.fit_transform(train_labels)
train_labels = train_labels.astype("float")
test_labels = le.transform(test_labels)

opt = SGD(lr=INIT_LR, momentum=0.9)
for _ in range(10):
    default_len = len(time_str)
    dup_dir = False
    while os.path.isdir(tmp_outdir):
        if 'dup_count' not in locals():
            dup_dir = True
            dup_count = 1
    
        new_time_str = time_str + "{:02d}".format(dup_count)
        tmp_outdir = (args['outdir'] +  new_time_str + '/')
                      
        dup_count += 1
    
    tmp_plotdir = tmp_outdir + args['plot']
    Path(tmp_outdir).mkdir(parents=True)
    
    with device('/gpu:' + str(args["gpu"])):
        model = GenericNet.build(*input_shape, classes, **model_params)
        model.compile(loss="categorical_crossentropy", optimizer=opt,
        	metrics=["accuracy"])
        
        #schedule = PolynomialDecay([i]maxEpochs=NUM_EPOCHS, initAlpha=INIT_LR,
        #        power=1.0)
        fname = os.path.sep.join([tmp_outdir,
                    "weights-genericnet-{epoch:03d}-{val_loss:.4f}.hdf5"])
        
        #callbacks = [LearningRateScheduler(schedule), ModelCheckpoint(fname)]
        callbacks = [ModelCheckpoint(fname,  monitor='val_loss', save_freq='epoch')]
        
        # train the network
        print("[INFO] training network...")
        H = model.fit(train_data, train_labels,
        	          validation_data=(test_data, test_labels), 
                      batch_size=BATCH_SIZE,
        	          epochs=NUM_EPOCHS, 
                      callbacks=callbacks,
                      verbose=1)
        # evaluate the network
        print("[INFO] evaluating network...")
        predictions = model.predict(test_data, batch_size=128)
        print(classification_report(test_labels.argmax(axis=1),
        	                        predictions.argmax(axis=1),
    	                        target_names=[str(x) for x in le.classes_]))
    
    # plot the training loss and accuracy
    
    save_vars = (model_params, H.history, time_str)
    
    with open(tmp_outdir + 'model_params.pkl', 'wb') as f:
        pickle.dump(save_vars, f)
    
    plt.style.use("ggplot")
    plt.figure()
    plt.plot(np.arange(0, NUM_EPOCHS), H.history["loss"], label="train_loss")
    plt.plot(np.arange(0, NUM_EPOCHS), H.history["val_loss"], label="val_loss")
    plt.plot(np.arange(0, NUM_EPOCHS), H.history["accuracy"], label="train_acc")
    plt.plot(np.arange(0, NUM_EPOCHS), H.history["val_accuracy"], label="val_acc")
    plt.title("Training Loss and Accuracy")
    plt.xlabel("Epoch #")
    plt.ylabel("Loss/Accuracy")
    plt.legend()
    plt.savefig(tmp_outdir + args["plot"])
# plt.show()
