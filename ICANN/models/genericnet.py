
# import the necessary packages
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense
from tensorflow.keras import backend as K

import pdb

class GenericNet:
        @staticmethod
        def build(width, height, depth, classes, 
                        conv_depths=[4, 8, 16, 32],
                        conv_pool=[True, True],
                        conv_shape=(3,3),
                        pool_shape=(2,2),
                        pool_stride=(2,2),
                        fc_depths=[64, 128, 256],
                        batch_norm=False):
                # initialize the model
                model = Sequential()

                if K.image_data_format() == "channels_first":
                        input_shape = (depth, height, width)
                else:
                    input_shape = (height, width, depth)

                # add convolutional layers
                first_depth = conv_depths[0]
                model.add(Conv2D(first_depth, conv_shape, 
                                 padding="same",
                                 activation="relu",
                                 input_shape=input_shape))
                if conv_pool[0] is True:
                    model.add(MaxPooling2D(pool_size=pool_shape, 
                                               strides=pool_stride))
                conv_depths = conv_depths[1:]
                conv_pool = conv_pool[1:]
                for (i, chann_depth) in enumerate(conv_depths):
                    model.add(Conv2D(chann_depth, conv_shape, 
                                     padding="same",
                                     activation="relu"))
                    if conv_pool[i] is True:
                        model.add(MaxPooling2D(pool_size=pool_shape, 
                                               strides=pool_stride))
                model.add(Flatten())

                # add fully connected layers
                for layer_depth in fc_depths:
                    model.add(Dense(layer_depth, activation="relu"))

                # add classification layer      
                model.add(Dense(classes, activation="softmax"))

                return model
