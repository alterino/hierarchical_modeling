from tensorflow.keras.preprocessing import image
from tensorflow.keras import backend
from shutil import copyfile
from data_processing import utils
from tensorflow.keras.models import Model

import numpy as np
import pickle
import datetime
import os
import json
import pdb
import h5py

from tensorflow.compat.v1.keras.backend import set_session
from tensorflow.compat.v1 import ConfigProto, Session

# to fix cudnn handle error
#config = ConfigProto()
#config.gpu_options.allow_growth = True
#sess = Session(config=config)
#set_session(sess)

def import_modules(model_name):
    global model_func
    global preprocess_input
    global decode_predictions
    
    if model_name is 'vgg16':
        from tensorflow.keras.applications.vgg16 import VGG16 as model_func
        from tensorflow.keras.applications.vgg16 import preprocess_input
        from tensorflow.keras.applications.vgg16 import decode_predictions
    elif model_name is 'resnet50':
        from tensorflow.keras.applications.resnet50 import ResNet50 as model_func
        from tensorflow.keras.applications.resnet50 import preprocess_input
        from tensorflow.keras.applications.resnet50 import decode_predictions
    elif model_name is 'inception_v3':
        from tensorflow.keras.applications.inception_v3 import InceptionV3 as model_func
        from tensorflow.keras.applications.inception_v3 import preprocess_input
        from tensorflow.keras.applications.inception_v3 import decode_predictions

def sort_data(data, ground_truth, labels):
    data_sorted = dict()
    for class_name in labels:
        data_sorted[class_name] = []
    for (idx,entry) in enumerate(data):
        data_sorted[labels[ground_truth[idx]]].append(list(entry))
    
    for k in data_sorted.keys():
        data_sorted[k] = np.array(data_sorted[k])
    
    return data_sorted

def gather_activations(base_model, data_in, layer_idxs):
    if type(data_in) is dict:
        activations = dict()
        for k in data_in.keys():
            activations[k] = []
            for idx, layer in enumerate(base_model.layers):
                if idx in layer_idxs:
                    model = Model(base_model.input, outputs=layer.output)
                    out = model.predict(data_in[k])
                    activations[k].append(out)
    else:
        activations = []
        for idx, layer in enumerate(base_model.layers):
            if idx in layer_idxs:
                model = Model(base_model.input, outputs=layer.output)
                out = model.predict(data_in)
                activations.append(out)

    return activations

class DataCollector:

    def __init__(self, 
            model_name,
            data_dir=os.path.expanduser('~/data/imagenet-data/raw-data/validation/'), 
            data_in=None,
            params=None, 
            layer_idxs=None,
            layer_dims = None,
            input_dims = (224, 224, 3),
            filepath_in = None,
            filepath_out = None,
            weights_path = None,
            log_path = 'data_log_' + datetime.datetime.now().strftime('%y%m%d%H%M%S') + '.txt'):
        
        if data_dir is None and data_in is None:
            raise ValueError('either data_dir or data_in must be specified')
        elif data_dir is not None and data_in is not None:
            raise ValueError('both data_dir and data_in cannot be specified')

        self.model_name = model_name
        self.weights_path = weights_path
        self.data_dir = data_dir
        self.data_in = data_in
        self.model_params = params
        self.layer_idxs = layer_idxs
        self.layer_dims = layer_dims
        self.input_dims = input_dims
        self.log_file = log_path
        self.filepath_out = filepath_out

        if filepath_in is None:
            self.initialize_Network_model()
        else:
            self.load_Activations(filepath_in)

    def initialize_Network_model(self):
        
        import_modules(self.model_name)
        if self.model_params is None:
                        
            self.model = self.model_func()
        else:
            self.model = model_func(**self.model_params)
        self.calculate_Activations()
        
    def calculate_Activations(self):
        with open(self.log_file, 'w+') as log_f:
            log_f.write('Processing run started %s' % datetime.datetime.now())
        model = self.model

        if self.weights_path is not None:
            model.load_weights(self.weights_path)
        if self.layer_idxs is None:
            self.layer_idxs = range(len(model.layers))
        if self.layer_dims is None:
            self.layer_dims = [model.layers[i].output_shape for i in self.layer_idxs]
            if type(self.layer_dims[0]) is list:
                self.layer_dims[0] = self.layer_dims[0][0]
            self.layer_dims = [dims[1:] for dims in self.layer_dims]

        layer_idxs = self.layer_idxs
        layer_dims = self.layer_dims
        layer_names = tuple([model.layers[i].name for i in layer_idxs])

        activations = dict()
        class_data = dict()
        if self.data_dir is not None:
            for root, dirs, _  in os.walk(self.data_dir):
                for directory in dirs:
                    class_key = directory
                    activations[class_key] = []
                    for class_dir, _, files in os.walk(os.path.join(root, directory)):
                        for dims in layer_dims:
                            activations[class_key].append(np.zeros( shape=((len(files),) + dims) ))
                        class_data[class_key] = np.zeros( shape=((len(files),) + self.input_dims) )
                        for idx, filename in enumerate(files):
                            datapoint_path = os.path.join(class_dir, filename)
                            # for now assuming 2D image input. Should be generalized to N dimensions
                            img = image.load_img(datapoint_path, 
                                                 target_size=self.input_dims)
                            datapoint = image.img_to_array(img)
                            datapoint = np.expand_dims(datapoint, axis=0)
                            datapoint = preprocess_input(datapoint)
                            class_data[class_key][idx,:] = datapoint
                        pdb.set_trace()
                        activations[class_key] = gather_activations(model, 
                                                                    class_data[class_key],
                                                                    self.layer_idxs)
                        print('completed directory: ' + class_dir)
        else:
            class_data = sort_data(self.data_in[0], self.data_in[1], self.data_in[2])
            activations = gather_activations(model, 
                                             class_data,
                                             self.layer_idxs)
        print('[INFO] ...calculations complete')
        pdb.set_trace()
#               for k in activations[class_key].keys():
#                   activations[class_key][k][idx]=layer_outs[int(k)]
        with open(self.log_file, 'a+') as log_f:
            log_f.write('\n%s:completed at %s' % (directory, datetime.datetime.now()))

        self.data = activations
        with open(self.log_file, 'a+') as log_f:
            log_f.write('\nCalculations completed at %s\n' % (datetime.datetime.now()))

        if self.filepath_out is not None:
            self.save_Activations()

    def load_Activations(self, filepath):
        activations = dict()
        dataset_name = 'activation_data'
        with h5py.File(filepath, 'r') as f:
            for synset_k in f[dataset_name]:
                activations[synset_k] = dict()
                for layer_k in f[dataset_name][synset_k]:
                    activations[synset_k][layer_k] = f[dataset_name][synset_k][layer_k][:]

        self.data = activations


    # data structure for hdf5 file could likely be improved in terms of compressability
    def save_Activations(self, filepath=None):
        out_data = utils.unfold_dict(self.data, key_substring='activation_data')
        pdb.set_trace()
        utils.write_to_hdf5(out_data, self.filepath_out)        



if __name__ == '__main__':
    data_path =  os.path.expanduser('~/data/imagenet-data/raw-data/validation/')
    model = 'vgg16'
    params = {'weights':'imagenet'}
#   load_path = os.path.expanduser('~/data/conceptors/trace_values/test_2.h5')

    data_collector = DataCollector(model, 
                                   data_dir=data_path, 
                                   params=params)
    pdb.set_trace()











