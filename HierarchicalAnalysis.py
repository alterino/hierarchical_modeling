import json
import data_processing as dp
from DataCollector import DataCollector
from ActivationMetrics import ActivationMetrics
import pdb, traceback, sys
import datetime


class HierarchicalAnalysis:

    def __init__(self,
                 model_name,
                 data_dir='/home/student/m/mimarino/data/imagenet-data/raw-data/validation/', 
                 params=None,
                 log_path = 'data_log_' + datetime.datetime.now().strftime('%y%m%d%H%M%S') + '.txt',
                 data_collector=None,
                 collector_params=None,
                 class_index_dict=None,
                 filepath_out=None,
                 filepath_in=None):
#       if data_collector is None:
#           self.data_collector = DataCollector(model_name, 
#                                               data_dir, 
#                                               params=params,
#                                               layer_idxs=layer_idxs,
#                                               layer_dims=layer_dims,
#                                               input_dims=input_dims,
#                                               log_path=log_path,
#                                               filepath_in=filepath_in)
#       else:
#           self.data_collector = data_collector
    
#       try:
#           if class_index_dict is None:
#               self.class_index_dict = dp.utils.get_default_class_dict()
#           else:
#               self.class_index_dict = class_index_dict
#       except:
#           extype, value, tb = sys.exc_info()
#           traceback.print_exc()
#           pdb.post_mortem()
#       
#       try:
#           self.extract_Trace_matrices()
#       except:
#           extype, value, tb = sys.exc_info()
#           traceback.print_exc()
#           pdb.post_mortem()

    def extract_Trace_matrices(self, normalize=True, white_noise=0.01):
        self.activation_metrics = ActivationsMetrics(self.data_collector.data, class_idx_dict)

def get_default_class_dict(filepath='imagenet_class_index.json'):
    with open(filepath) as f:
        class_index_dict = json.load(f)

    return class_index_dict
    

if __name__ == '__main__':
    hier_analysis = HierarchicalAnalysis('vgg16')
    
    hier_analysis.activation_metrics.export_Instance_data('test.pkl')
    
    pdb.set_trace()

