#!/bin/bash

echo "*** Start of SGE job ***"
date
echo ""
echo "Hostname"
echo "$HOSTNAME"
echo 'Job-ID:'
echo $JOB_ID
echo ""
echo "Start"
echo ""

SGE_TASK_ID=$1
SGE_TASK_STEPSIZE=$2

ROW_START=$(( $SGE_TASK_ID-1 ))
ROW_IDX=$(( $ROW_START ))
ROW_END=$(( $ROW_START+$SGE_TASK_STEPSIZE )) 

while [ $ROW_IDX -lt $ROW_END ]; do
    python run_cosine_calculations_distributed.py $ROW_IDX
    ROW_IDX=$(( $ROW_IDX+1 ))
done

echo "Completed job from idx: "
echo $ROW_START
echo "to idx:"
echo $ROW_END
