import os
import pdb
import time
import h5py
import utils
import json

from nltk.corpus import wordnet as wn
from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.spatial.distance import squareform
from matplotlib import pyplot as plt

import misc.pkl_utils

#import sys
#sys.setrecursionlimit(1500)

plotit = True
num_layers = 49

#class_index_filepath = os.path.expanduser('~/.keras/models/imagenet_class_index.json')
class_index_filepath = 'imagenet_class_index.json'
with open(class_index_filepath) as f:
    class_index_dict = json.load(f)

data_dir = os.path.expanduser('~/data/conceptors/analysis/')
fn = 'resnet50_imagenet.hdf5'

# data_collection_name = 'cosine_vals/vgg16_imagenet/'
sim_mats = dict()
dist_mats = dict()
dist_vec = dict()
clustering = dict()


labelList = range(1,1001)

with h5py.File(data_dir + fn, 'r') as f:
    for k in f:
        sim_mats[k] = f[k][:]
        dist_mats[k] = 1 - sim_mats[k]
        dist_vec[k] = squareform(dist_mats[k])
        clustering[k] = linkage(dist_vec[k],
                                method='average')
        if plotit == True:
            plt.figure(figsize=(10,7))
            dendrogram(clustering[k],
                       orientation='top',
                       labels=labelList,
                       distance_sort='descending',
                       show_leaf_counts=True)
            plt.title('agglomerative clustering for layer %s' % k)
            plt.show()
print('extracting base hierarchy... ', end='')
base_hierarchy = utils.extract_base_hierarchy(class_index_dict)
print('complete.')

nn_hierarchy = dict()
return_labels = True
sim_dict = dict()
f1_vec = list()
recall_vec = list()
precision_vec = list()
for k in sorted(clustering.keys()):
    if int(k) < 30:
        continue
    print('layer ' + k)
    print('parsing linkage... ', end='')
    nn_hierarchy[k] = utils.parse_linkage(clustering[k], return_labels, class_index_dict)
    print('complete.')
    name_count = 0
    tmp_list = []
    sim_dict[k] = dict()
    for kb in sorted(base_hierarchy.keys()):
        group_B = base_hierarchy[kb]
        synset_B = wn._synset_from_pos_and_offset(kb[0], int(kb[1:]))
        depth_B = synset_B.max_depth()
        sim_dict[k][kb] = dict()
        max_F1 = 0
        for kn in reversed(sorted(nn_hierarchy[k].keys())):
            if int(kn) < 1000:
                continue
            group_A_inids = nn_hierarchy[k][kn]['leaves']
            group_A = utils.inid_to_wnid(group_A_inids, class_index_dict)
            depth_A = nn_hierarchy[k][kn]['node_depth']
            
            depths = (depth_A+1, depth_B+1)
            get_full_stats = True
            stats = utils.class_similarity(group_A, group_B, get_full_stats)
            F1 = stats[0]
            pdb.set_trace()
            if F1 > max_F1:
                max_F1 = F1
                recall = stats[1]
                precision = stats[2]
                tp = set(group_A) & set(group_B)
                fp = set(group_A) - set(group_B)
                fn = set(group_B) - set(group_A)
                matching_group = group_A
                matching_group_inids = group_A_inids
                match_depth = depth_A
                sim_dict[k][kb]['max_similarity'] = max_F1
                sim_dict[k][kb]['closest_cluster'] = matching_group
                sim_dict[k][kb]['tp'] = tp
                sim_dict[k][kb]['fp'] = fp
                sim_dict[k][kb]['fn'] = fn
                sim_dict[k][kb]['recall'] = recall
                sim_dict[k][kb]['precision'] = precision

        group_name = synset_B.name()
        matching_leaves = utils.inid_to_semantic_label(matching_group_inids, class_index_dict)
        if len(matching_leaves) > 100 and len(matching_leaves) < 1000:
            print('-----------------------------------------------------------------')
            print('wordnet depth = %i, agglomerative depth = %i' % (depth_B, match_depth))
            print('Best match for cluster %s' % group_name)
            print('similarity = %.3f' % max_F1)
            utils.print_list_to_terminal( matching_leaves, 3)
            print('wordnet depth = %i, agglomerative depth = %i' % (depth_B, match_depth))
            print('Best match for cluster %s' % group_name)
            print('similarity = %.3f, results for layer %s, class length = %i' % (max_F1, k,
                len(matching_leaves)))
            print('-----------------------------------------------------------------')
            pdb.set_trace()

data_filepath = os.path.expanduser('~/tmp/data/conceptors/analysis/presentation_data.hdf5')
misc.pkl_utils.save_obj(sim_dict, data_filepath) 
            
#       depth = nn_hierarchy[k][k1]['node_depth']
#       member_count = nn_hierarchy[k][k1]['member_count']
#       print('cluster info for group %04i, depth = %i' % (int(k1), depth))
#       print('----------------------------------------------------\n')
#       for entry, has_more in utils.lookahead(nn_hierarchy[k][k1]['semantic_labels']):
#           tmp_list.append(entry)
#           name_count = name_count + 1    
#           if name_count > 2 or not has_more:
#               for label, not_end in utils.lookahead(tmp_list):
#                   label = label[1]
#                   if not_end:
#                       print(label.ljust(30), end = '')
#                   else:
#                       print(label)
#               tmp_list = []
#               name_count = 0
#       print('\n')
#       print('cluster info for group %04i, depth = %i, member_count = %i' 
#                                               % (int(k1), depth, member_count))
#       print('____________________________________________________\n')
#       print('\n')
#       pdb.set_trace()
