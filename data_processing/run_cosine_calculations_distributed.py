import os
import pdb
import numpy as np
import sys
import conceptors as C
from doodle_script import write_file_for_stuff

import json
import time

from misc.pkl_utils import save_obj
from misc.pkl_utils import load_obj

row_idx = int(sys.argv[1])

root_path = os.path.expanduser('~/data/conceptors/')
# data_dirpath = root_path + 'working_data/'
data_dirpath = root_path + 'correlation_matrices/resnet50/'
out_dirpath = os.path.expanduser('~/data/conceptors/analysis/resnet50/')

class_index_filepath = os.path.expanduser('~/.keras/models/imagenet_class_index.json')

with open(class_index_filepath) as f:
    class_index_dict = json.load(f)

idx = 0
cos_matrix = dict()
start_time = time.time()
for root, _, filenames in os.walk(data_dirpath):
    filenames = sorted(filenames)
    elapsed_time = int( time.time() - start_time )
    print('beginning calculation set %i,  elapsed time = %i' % (row_idx, elapsed_time) ) 
    fn1 = filenames[row_idx]
    R_class1 = load_obj( root + fn1 )
    for j in range(row_idx+1,len(filenames)):
        cos_values = []
        idxs_str = str(row_idx).zfill(3) + '_' + str(j).zfill(3)
        temp_filestr = out_dirpath + idxs_str + '.txt'
        if os.path.isfile(temp_filestr):
            continue
        fn2 = filenames[j]
        R_class2 = load_obj( root + fn2 )
        for i, R_1 in enumerate(R_class1):
            R_2 = R_class2[i]
            cos_values.append(C.generalized_cosine(R_1, R_2))
        with open(temp_filestr, 'w') as f:
            for layer_idx, sim_val in enumerate(cos_values):
                f.write(str(layer_idx).zfill(3) + ':' + str(sim_val) + '\n')
# write_file_for_stuff(key_list_by_matrix_index)
