import os
import pdb
import numpy as np
import conceptors as C
from doodle_script import write_file_for_stuff

import json
import time

from pkl_utils import save_obj
from pkl_utils import load_obj

root_path = os.path.expanduser('~/tmp/data/conceptors/')
# data_dirpath = root_path + 'working_data/'
data_dirpath = root_path + 'correlation_matrices/resnet50/'
out_dirpath = os.path.expanduser('~/tmp/data/conceptors/analysis/resnet50/')

class_index_filepath = os.path.expanduser('~/.keras/models/imagenet_class_index.json')

with open(class_index_filepath) as f:
    class_index_dict = json.load(f)

idx = 0
cos_matrix = dict()
start_time = time.time()
for root, _, filenames in os.walk(data_dirpath):
    key_list_by_matrix_index = [''] * len(filenames)
    cos_matrix['fc1'] = np.zeros( [len(filenames), len(filenames)] )
    cos_matrix['fc2'] = np.zeros( [len(filenames), len(filenames)] )
    filenames = sorted(filenames)
    for i in range(len(filenames)):
        elapsed_time = int( time.time() - start_time )
        print('beginning calculation set %i,  elapsed time = %i' % (i, elapsed_time) ) 
        fn1 = filenames[i]
        print('filename = ' + fn1)
        key_list_by_matrix_index[i] = [str(int(s)) for s in fn1[:-4].split('_') if s.isdigit()][-1]
        dict_class1 = load_obj( root + fn1 )
        fc1_A = dict_class1['fc1']
        fc2_A = dict_class1['fc2']
        for j in range(i+1,len(filenames)):
            idxs_str = str(i).zfill(3) + '_' + str(j).zfill(3)
            temp_filestr = out_dirpath + idxs_str + '.txt'
            if os.path.isfile(temp_filestr):
                continue
            with open(temp_filestr, 'w') as f:
                fn2 = filenames[j]
                dict_class2 = load_obj( root + fn2 )
                fc1_B = dict_class2['fc1']
                fc2_B = dict_class2['fc2']
                cos_matrix['fc1'][i][j] = C.generalized_cosine(fc1_A, fc1_B)
                cos_matrix['fc1'][j][i] = cos_matrix['fc1'][i][j]
                cos_matrix['fc2'][i][j] = C.generalized_cosine(fc2_A, fc2_B)
                cos_matrix['fc2'][j][i] = cos_matrix['fc2'][i][j]
                print('idx: ' + str(i) + ',' + str(j) + 
                      ' fc1: ' + str(cos_matrix['fc1'][i][j]) + 
                      ' fc2: ' + str(cos_matrix['fc2'][i][j]))
                for k in cos_matrix:
                    f.write(k + ':' + str(cos_matrix[k][i][j]) + '\n')
#write_file_for_stuff(key_list_by_matrix_index)
