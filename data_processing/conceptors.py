import numpy as np
import pdb
import time
from numpy.core.umath_tests import inner1d

def calc_conceptor(R,a):
    #return np.dot(R,np.linalg.inv(R + a*np.ones(np.shape(R))))
    return np.dot(R,np.linalg.inv(R+a**(-1)*np.diag(np.ones(np.shape(R)[0]))))

def calc_R(x,normieren=True,addWhiteNoise=0.01):
    if normieren == True:
        x = zeilen_normieren(x)
    result = np.tensordot(x,x,axes=[0,0]) / np.shape(x)[0]
    if addWhiteNoise > 0:
        unit = np.diag(np.ones(np.shape(x)[1]))
        result = (1-addWhiteNoise)*result + addWhiteNoise/np.shape(x)[1]*unit
    return result

def zeilen_normieren(x):
    return x / np.sqrt(np.sum(x**2,axis=1)[:,np.newaxis])

def conc_and(A,B):
    n = np.shape(A)[0]
    return np.linalg.inv(np.linalg.inv(A)+np.linalg.inv(B)-np.diag(np.ones(n)))

def conc_not(A):
    n = np.shape(A)[0]
    return np.diag(np.ones(n))-A

def conc_or(A,B):
    n = np.shape(A)[0]
    unit = np.diag(np.ones(n))
    return unit - np.linalg.inv(np.linalg.inv(unit-A) + np.linalg.inv(unit-B) - unit)

def scale_apert(C,gamma):
    n = np.shape(C)[0]
    unit = np.diag(np.ones(n))
    return np.dot(C,np.linalg.inv(C+1./gamma*(unit - C)))

def calc_apert(C):
    n = np.shape(C)[0]
    unit = np.diag(np.ones(n))
    R = np.linalg.inv(unit - C) - unit
    return np.trace(R)

def is_response(C,x):
    return np.dot(np.transpose(x),np.dot(C,x))/np.dot(np.transpose(x),x)

def linear_blending(A,B,alpha,beta):
    return (alpha*A + beta*B)/(alpha + beta)

def weighted_or(A,B,alpha,beta):
    gamma = alpha + beta
    return conc_or(scale_apert(A,alpha/gamma),scale_apert(B,beta/gamma))

def generalized_cosine(A,B,speedup=True):
    cos_AB = np.sum(inner1d(A, B)) / \
                      np.sqrt( np.sum(inner1d(A, A)) * np.sum(inner1d(B, B)) )
    return cos_AB


if __name__ == "__main__":
    xA = np.array([
        [1.0,0.1,1.0],
        [0.1,1.0,0.3],
        [1.0,0.1,0.9],
        [0.1,1.0,0.2]
        ])
    RA = calc_R(xA)
    CA = calc_conceptor(RA,20)
    
    xB = np.array([
        [0.8,0.1,0.1],
        [0.7,0.1,0.1],
        [0.1,0.2,0.2]
    ])
    RB = calc_R(xB)
    CB = calc_conceptor(RB,20)
    
    xC = np.array([
        [0.1,0.7,0.8],
        [0.2,0.9,0.6],
        [0.7,0.3,0.2],
        [0.8,0.4,0.1]
    ])
    RC = calc_R(xC)
    CC = calc_conceptor(RC,20)
    
    C1 = conc_and(scale_apert(CA,2),conc_or(CB,CC))
    C2 = conc_or(conc_and(CA,CB),conc_and(CA,CC))
    
    print(C1-C2)
    w,v = np.linalg.eigh(C1-C2)
    print(w)
    
    notCA = conc_not(CA)
    notCB = conc_not(CB)
    
    CAandCB = conc_and(CA,CB)
    CAminusCB = conc_and(CA,conc_not(CB))
    CBminusCA = conc_and(CB,conc_not(CA))
    
    test1 = np.array([0.1,0.7,0.7])
    test2 = np.array([0.6,0.1,0.6])
    
    print(calc_apert(linear_blending(CA,CB,0.2,0.8)))
    print(calc_apert(weighted_or(CA,CB,0.2,0.8)))
    print(calc_apert(CA))
