#!/bin/bash

LISTFILE=synset_grouplist_sorted.txt
FILE_PREFIX=R_synset
SOURCE_PATH=$HOME/tmp/data/conceptors/analysis/correlation_matrices
DEST_PATH=$HOME/tmp/data/conceptors/analysis/working_data

while read p; do
    SYNSET_ID=$(printf "%03d" $p)
    FILENAME="${FILE_PREFIX}_${SYNSET_ID}.pkl"
    FULL_SOURCEPATH=$SOURCE_PATH/$FILENAME
    if [ -f $FULL_SOURCEPATH ]; then
        ln -s $FULL_SOURCEPATH $DEST_PATH
    fi
done < $LISTFILE
