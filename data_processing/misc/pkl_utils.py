import pickle

def search(values, target):
    for k in values:
        for v in values[k]:
            if target in v:
                return k
    return None

def save_obj(obj, name):
    with open(name, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    with open( name, 'rb') as f:
        return pickle.load(f)

def backup_dictfile(name):
    src = 'obj/' + name + '.pkl'
    dst = 'obj/tmp_check/' + name + '.pkl'
    copyfile(src, dst)
