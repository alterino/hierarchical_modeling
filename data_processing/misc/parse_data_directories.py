import os
import pdb
import json

def search(values, target):
    for k in values:
        for v in values[k]:
            if target in v:
                return k
    return None



class_index_filepath = os.path.expanduser('~/.keras/models/imagenet_class_index.json')
data_dir = os.path.expanduser('~/tmp/imagenet-data/raw-data/validation/')

with open(class_index_filepath) as f:
    class_index_dict = json.load(f)

for root, dirs, _  in os.walk(data_dir):
    for directory in dirs:
        print directory
        class_key = search(class_index_dict, directory)
        pdb.set_trace()
        for root,_,files in os.walk(os.path.join(root, directory)):
            for filename in files:
                print(os.path.join(root, directory, filename))
                pdb.set_trace()


