#!/bin/bash

# this script parses .txt files containing data from similarity calculations and checks for file
# integrity according to assuming a text file of the form
# fc1: $DECIMAL_NUMBER
# fc2: $DECIMAL_NUMBER

DIR="$HOME/tmp/data/conceptors/tmp/" 
ERROR_LOG="error_log.txt"
SUCCESS_LOG="check_tmp_files_log.txt"
touch $SUCCESS_LOG
IDX=0

while [ $IDX -lt 999 ]; do
    IDX_STR=$(printf "%03d" $IDX)
    FILE_PATTERN="${DIR}${IDX_STR}*"
    for filename in $FILE_PATTERN; do
        FC_NUM=0
        FIRST_LINE_PASS=false
        SECOND_LINE_PASS=false
        while read p; do
            FC_NUM=$(( $FC_NUM+1 ))
            TEXT_PATTERN="fc${FC_NUM}:[0-1].[0-9]+"
            if ! [[ $p =~ $TEXT_PATTERN ]]
            then
                break
            else
               if [[ "$FC_NUM" == 1 ]]
               then
                  FIRST_LINE_PASS=true 
               elif [[ "$FC_NUM" == 2 && "$FIRST_LINE_PASS" == true ]]
               then
                   SECOND_LINE_PASS=true
               else
                   echo "entered condition that should not exist @ filename: $filename"
                   exit
               fi
           fi
       done < $filename
       if [[ "$FIRST_LINE_PASS" == true && "$SECOND_LINE_PASS" == true ]]
       then
           echo "file $filename passed pattern test" >> "$SUCCESS_LOG" 
       else
           echo "****file $filename corrupted****"
           echo "$filename" >> $ERROR_LOG
           rm $filename
           echo "*************       beginning new calculation cycle for row $IDX_STR       *************"
           source ~/.virtualenvs/tensorflow/bin/activate
           python run_cosine_calculations_distributed.py $IDX
           deactivate
       fi
   done
   IDX=$(( $IDX+1 ))
done
