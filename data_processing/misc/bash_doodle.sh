#!/bin/bash

# data_dir=$HOME/Projects/hidden_ontologies/tmp/
data_dir=$HOME/tmp/data/conceptors/tmp/*.txt

for filename in $data_dir; do
    idx_str="${filename#.*}"
    row_idx="${filename%_*}"
    col_idx="${filename#*_}"

    echo $row_idx
    echo $col_idx

done
