#!/bin/bash

source ~/.virtualenvs/tensorflow/bin/activate

echo 'clustering calculations beginning'
date

MIN_CLUSTERS=2
MAX_CLUSTERS=12
NUM_CLUSTERS=$MIN_CLUSTERS

while [ $NUM_CLUSTERS -lt $(( $MAX_CLUSTERS+1 )) ]; do
    python parse_tmp_files.py $NUM_CLUSTERS
    NUM_CLUSTERS=$(( $NUM_CLUSTERS+1 ))
done

deactivate

echo 'clustering calculations complete'
date
